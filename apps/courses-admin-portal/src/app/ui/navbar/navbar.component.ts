import { Component, Input, OnInit } from '@angular/core'

@Component({
  selector: 'nextlevel-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {
  collapsed = false
  @Input() title: string

  constructor() {}

  ngOnInit(): void {}
}
