import { BrowserAnimationsModule } from '@angular/platform-browser/animations'
import { NgModule } from '@angular/core'
import { RouterModule } from '@angular/router'
import { ToastrModule } from 'ngx-toastr'
import { AppComponent } from './app.component'
import { AppRoutes } from './app.routing'
import { NxModule } from '@nrwl/angular'
import { BrowserModule } from '@angular/platform-browser'
import { authRoutes, AuthModule } from '@nextlevel/auth'
import { StoreModule } from '@ngrx/store'
import { EffectsModule } from '@ngrx/effects'
import { StoreDevtoolsModule } from '@ngrx/store-devtools'
import { environment } from '../environments/environment'
import { NgbModule } from '@ng-bootstrap/ng-bootstrap'
// import { StoreRouterConnectingModule } from '@ngrx/router-store'
import { NavbarComponent } from './ui/navbar/navbar.component'
import { DashboardComponent } from './pages/dashboard/dashboard.component'

@NgModule({
  declarations: [AppComponent, NavbarComponent, DashboardComponent],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    RouterModule.forRoot(AppRoutes, {
      useHash: true
    }),
    NxModule.forRoot(),
    ToastrModule.forRoot({
      timeOut: 2000,
      positionClass: 'toast-top-right',
      preventDuplicates: false,
      maxOpened: 5
    }),
    AuthModule.forRoot(environment),
    NgbModule,
    StoreModule.forRoot({}),
    EffectsModule.forRoot([]),
    environment.production ? [] : StoreDevtoolsModule.instrument()
    // StoreRouterConnectingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
