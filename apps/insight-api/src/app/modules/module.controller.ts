import { Body, Controller, Get, Post, Put, Request, UseGuards } from '@nestjs/common'
import { CreateModuleDto } from './dto/create-module.dto'
import { UpdateModuleDto } from './dto/update-module.dto'
import { ModuleDecorator } from './decorators/module.decorator'
import { Logger } from '@nestjs/common'
import { ModuleService } from './entity/module.service'
import { JwtAuthGuard } from '../auth/guards/jwt.auth-guard'
import { Module } from '@nextlevel/domain'
import { ModuleEntity } from './entity/module.entity'

@Controller('modules')
export class ModuleController {
  private TAG: string = ModuleController.name.toString()

  constructor(private readonly moduleService: ModuleService) {}

  @Post()
  @UseGuards(JwtAuthGuard)
  create(@Body() moduleDto: CreateModuleDto): Promise<Module> {
    return this.moduleService.create(moduleDto).then(
      (module) =>
        ({
          ...module.toJson()
        } as Module)
    )
  }

  @UseGuards(JwtAuthGuard)
  @Get()
  getAll(@ModuleDecorator() module: any): Promise<Module[]> {
    Logger.debug('getAll called', this.TAG)
    return this.moduleService.findAll()
  }

  @UseGuards(JwtAuthGuard)
  @Put()
  async putmodule(@ModuleDecorator() module: any, @Body() properties: UpdateModuleDto): Promise<Module> {
    const updated = await this.moduleService.update(module.getId(), properties)

    return {
      ...updated.toJson()
    }
  }
}
