import { Node } from 'neo4j-driver'
import { Module } from '@nextlevel/domain'

export class ModuleEntity {
  constructor(private readonly node: Node) {}

  getId(): string {
    return (<Record<string, any>>this.node.properties).id
  }

  getName(): string {
    return (<Record<string, any>>this.node.properties).name
  }

  getDescription(): string {
    return (<Record<string, any>>this.node.properties).description
  }

  toJson(): Module {
    const { ...properties } = <Record<string, any>>this.node.properties

    return properties as Module
  }
}
