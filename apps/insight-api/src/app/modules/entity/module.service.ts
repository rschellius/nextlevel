import { Injectable } from '@nestjs/common'
import { Module } from '@nextlevel/domain'
import { Neo4jService } from 'nest-neo4j'
import { CreateModuleDto } from '../dto/create-module.dto'
import { UpdateModuleDto } from '../dto/update-module.dto'
// import { EncryptionService } from '../encryption/encryption.service'
import { ModuleEntity } from './module.entity'

@Injectable()
export class ModuleService {
  constructor(private readonly neo4jService: Neo4jService) {}

  findAll(): Promise<Module[] | any[]> {
    return (
      this.neo4jService
        // MATCH (u:Module)
        // RETURN u
        // LIMIT 25
        .read(
          `
            MATCH
            (m: Module)-[:REALIZES]->(o: Objective),
            (m)-[:REALIZES]->(k: KnowledgeItem)--(a: KnowledgeArea)

            RETURN
            m as module,
            o as objective,
            k as knowledgeItem,
            a as knowledgeArea
          `
        )
        .then((res) => {
          const moduleRecs = res.records.map((item) => item.get('module'))
          // const objectivesRecs = res.records.map((item) => item.get('o'))
          // const knowledgeItemRecs = res.records.map((item) => item.get('k'))
          // const knowledgeAreaRecs = res.records.map((item) => item.get('a'))

          const modulesMap = new Map()
          moduleRecs.forEach((item: Module) => {
            if (!modulesMap.has(item.identity.low)) modulesMap.set(item.identity.low, item)
          })

          const objectivesMap = new Map()
          // const objectivesRecs = res.records.map((item) => item.get('objective'))
          // res.records.forEach((item) => if(!objectivesMap.has({item.})))

          let modules = Array.from(modulesMap.values())
          modules = modules.map((module) => {
            return {
              ...module
              // objectives: objectivesRecs as any[]
            }
          })

          return res.records
        })
    )
  }

  find(email: any): Promise<ModuleEntity | undefined> {
    return this.neo4jService
      .read(
        `
        MATCH (n:Module)-[:REALIZES]->(o: Objective)

        RETURN n,
          o as objectives,
        LIMIT 25
        `,
        { email }
      )
      .then((res) => (res.records.length ? new ModuleEntity(res.records[0].get('u')) : undefined))
  }

  async create(module: CreateModuleDto): Promise<ModuleEntity> {
    console.log('create', module)
    return this.neo4jService
      .write(
        `
            CREATE (u:Module { name: $name, id: randomUUID() })
            SET u += $properties
            RETURN u
        `,
        {
          properties: {
            ...module
          },
          name: module.name
        }
      )
      .then((res) => {
        console.log('success')
        return new ModuleEntity(res.records[0].get('u'))
      })
    // .catch((error) => console.log('error', error))
  }

  update(id: string, properties: UpdateModuleDto): Promise<ModuleEntity> {
    return this.neo4jService
      .write(
        `
            MATCH (u:Module { id: $id })
            SET u += $properties
            RETURN u
        `,
        { id, properties }
      )
      .then((res) => new ModuleEntity(res.records[0].get('u')))
  }
}
