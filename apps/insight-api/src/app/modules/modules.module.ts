import { Module } from '@nestjs/common'
import { ConfigModule, ConfigService } from '@nestjs/config'
// import { JwtModule } from '@nestjs/jwt'
// import { PassportModule } from '@nestjs/passport'

import { Neo4jService } from 'nest-neo4j'
import { environment } from '../../environments/environment'
import { ModuleService } from './entity/module.service'
import { ModuleController } from './module.controller'
// import { AuthController } from './auth.controller'
// import { AuthService } from './auth.service'
// import { JwtStrategy } from './guards/jwt.strategy'
// import { LocalStrategy } from './guards/local.strategy'

@Module({
  imports: [],
  providers: [ModuleService],
  controllers: [
    ModuleController
    // UserController, UsersController, ProfileController
  ],
  exports: []
})
export class ModulesModule {
  constructor(private readonly neo4jService: Neo4jService) {}
}
