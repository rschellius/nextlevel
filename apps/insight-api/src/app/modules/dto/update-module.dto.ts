import { IsNotEmpty, IsEmail } from 'class-validator'

export class UpdateModuleDto {
  @IsNotEmpty()
  name: string

  description: string
}
