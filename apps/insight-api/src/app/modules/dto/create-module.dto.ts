import { IsNotEmpty, IsEmail } from 'class-validator'

export class CreateModuleDto {
  @IsNotEmpty()
  name: string

  @IsNotEmpty()
  description: string
}
