import { createParamDecorator, ExecutionContext } from '@nestjs/common'
import { ModuleEntity } from '../entity/module.entity'

export const ModuleDecorator = createParamDecorator(
  (data: unknown, ctx: ExecutionContext): ModuleEntity => {
    const request = ctx.switchToHttp().getRequest()
    return request.Module
  }
)
