import { Module, OnModuleInit } from '@nestjs/common'
import { ConfigModule } from '@nestjs/config'
import { Neo4jConfig, Neo4jModule, Neo4jService } from 'nest-neo4j/dist'
import { environment } from '../environments/environment'
import { AppController } from './app.controller'
import { AppService } from './app.service'
import { AuthModule } from './auth/auth.module'
import { KnowledgeModule } from './boks/knowledgemodule'
import { EventsModule } from './events/events.module'
import { ModulesModule } from './modules/modules.module'
import { ServeStaticModule } from '@nestjs/serve-static'
import { join } from 'path'

@Module({
  imports: [
    ConfigModule.forRoot({ isGlobal: true }),
    Neo4jModule.forRoot(environment as Neo4jConfig),
    AuthModule,
    KnowledgeModule,
    ModulesModule,
    EventsModule,
    ServeStaticModule.forRoot({
      rootPath: join(__dirname, '..', 'insight-admin'),
      exclude: ['/insight-api*']
    })
  ],
  controllers: [],
  providers: []
})
export class AppModule implements OnModuleInit {
  constructor(private readonly neo4jService: Neo4jService) {}

  onModuleInit() {
    return Promise.all([
      this.neo4jService.write('CREATE CONSTRAINT ON (u:User) ASSERT u.id IS UNIQUE').catch((e) => {}),
      this.neo4jService.write('CREATE CONSTRAINT ON (u:User) ASSERT u.email IS UNIQUE').catch((e) => {})
    ])
  }
}
