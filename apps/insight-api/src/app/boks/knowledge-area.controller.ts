import { Body, Controller, Get, Post, Put, Request, UseGuards } from '@nestjs/common'
import { CreateKnowledgeAreaDto } from './dto/create-knowledge-area.dto'
import { UpdateKnowledgeAreaDto } from './dto/update-knowledge-area.dto'
import { KnowledgeAreaDecorator } from './decorators/knowledge-area.decorator'
import { Logger } from '@nestjs/common'
import { KnowledgeAreaService } from './entity/knowledge-area.service'
import { JwtAuthGuard } from '../auth/guards/jwt.auth-guard'
import { KnowledgeArea } from '@nextlevel/domain'
import { KnowledgeAreaEntity } from './entity/knowledge-area.entity'

@Controller('knowledgearea')
export class KnowledgeAreaController {
  constructor(private readonly knowledgeAreaService: KnowledgeAreaService) {}

  @Post()
  @UseGuards(JwtAuthGuard)
  create(@Body() knowledgeAreaDto: CreateKnowledgeAreaDto): Promise<KnowledgeArea> {
    return this.knowledgeAreaService.create(knowledgeAreaDto).then(
      (knowledgeArea) =>
        ({
          ...knowledgeArea.toJson()
        } as KnowledgeArea)
    )
  }

  @UseGuards(JwtAuthGuard)
  @Get()
  getAll(@KnowledgeAreaDecorator() knowledgeArea: any): Promise<KnowledgeAreaEntity[]> {
    Logger.debug('getKnowledgeArea called')
    return this.knowledgeAreaService.findAll().then((items) => {
      return items
    })
  }

  @UseGuards(JwtAuthGuard)
  @Put()
  async putKnowledgeArea(
    @KnowledgeAreaDecorator() knowledgeArea: any,
    @Body() properties: UpdateKnowledgeAreaDto
  ): Promise<KnowledgeArea> {
    const updated = await this.knowledgeAreaService.update(knowledgeArea.getId(), properties)

    return {
      ...updated.toJson()
    }
  }
}
