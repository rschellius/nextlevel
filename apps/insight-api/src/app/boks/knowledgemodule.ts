import { Module } from '@nestjs/common'
import { ConfigModule, ConfigService } from '@nestjs/config'
// import { JwtModule } from '@nestjs/jwt'
// import { PassportModule } from '@nestjs/passport'

import { Neo4jService } from 'nest-neo4j'
import { environment } from '../../environments/environment'
import { KnowledgeAreaService } from './entity/knowledge-area.service'
import { KnowledgeAreaController } from './knowledge-area.controller'
// import { AuthController } from './auth.controller'
// import { AuthService } from './auth.service'
// import { JwtStrategy } from './guards/jwt.strategy'
// import { LocalStrategy } from './guards/local.strategy'

@Module({
  imports: [],
  providers: [KnowledgeAreaService],
  controllers: [
    KnowledgeAreaController
    // UserController, UsersController, ProfileController
  ],
  exports: []
})
export class KnowledgeModule {
  constructor(private readonly neo4jService: Neo4jService) {}
}
