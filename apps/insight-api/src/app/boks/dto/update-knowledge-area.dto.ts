import { IsNotEmpty, IsEmail } from 'class-validator'

export class UpdateKnowledgeAreaDto {
  @IsNotEmpty()
  name: string

  description: string
}
