import { IsNotEmpty, IsEmail } from 'class-validator'

export class CreateKnowledgeAreaDto {
  @IsNotEmpty()
  name: string

  @IsNotEmpty()
  description: string
}
