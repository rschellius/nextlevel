import { Injectable } from '@nestjs/common'
import { KnowledgeArea } from '@nextlevel/domain'
import { Neo4jService } from 'nest-neo4j/dist'
import { CreateKnowledgeAreaDto } from '../dto/create-knowledge-area.dto'
import { UpdateKnowledgeAreaDto } from '../dto/update-knowledge-area.dto'
// import { EncryptionService } from '../encryption/encryption.service'
import { KnowledgeAreaEntity } from './knowledge-area.entity'

@Injectable()
export class KnowledgeAreaService {
  constructor(
    private readonly neo4jService: Neo4jService // private readonly encryptionService: EncryptionService
  ) {}

  findAll(): Promise<KnowledgeAreaEntity[] | undefined> {
    return (
      this.neo4jService
        .read(
          `
            MATCH (u:KnowledgeArea)
            RETURN u
            LIMIT 25
        `
        )
        // .then((res) => (res.records.length ? new KnowledgeAreaEntity(res.records[0]) : undefined))
        .then((res) =>
          res.records.length
            ? res.records.map((record) => new KnowledgeAreaEntity(record.get('u')))
            : undefined
        )
    )
  }

  find(email: any): Promise<KnowledgeAreaEntity | undefined> {
    return this.neo4jService
      .read(
        `
            MATCH (u:KnowledgeArea {email: $email})
            RETURN u
        `,
        { email }
      )
      .then((res) => (res.records.length ? new KnowledgeAreaEntity(res.records[0].get('u')) : undefined))
  }

  async create(knowledgeArea: CreateKnowledgeAreaDto): Promise<KnowledgeAreaEntity> {
    console.log('create', knowledgeArea)
    return this.neo4jService
      .write(
        `
            CREATE (u:KnowledgeArea { name: $name, id: randomUUID() })
            SET u += $properties
            RETURN u
        `,
        {
          properties: {
            ...knowledgeArea
          },
          name: knowledgeArea.name
        }
      )
      .then((res) => {
        console.log('success')
        return new KnowledgeAreaEntity(res.records[0].get('u'))
      })
    // .catch((error) => console.log('error', error))
  }

  update(id: string, properties: UpdateKnowledgeAreaDto): Promise<KnowledgeAreaEntity> {
    return this.neo4jService
      .write(
        `
            MATCH (u:KnowledgeArea { id: $id })
            SET u += $properties
            RETURN u
        `,
        { id, properties }
      )
      .then((res) => new KnowledgeAreaEntity(res.records[0].get('u')))
  }
}
