import { Node } from 'neo4j-driver'
import { KnowledgeArea } from '@nextlevel/domain'

export class KnowledgeAreaEntity {
  constructor(private readonly node: Node) {}

  getId(): string {
    return (<Record<string, any>>this.node.properties).id
  }

  getName(): string {
    return (<Record<string, any>>this.node.properties).name
  }

  getDescription(): string {
    return (<Record<string, any>>this.node.properties).description
  }

  toJson(): KnowledgeArea {
    const { ...properties } = <Record<string, any>>this.node.properties

    return properties as KnowledgeArea
  }
}
