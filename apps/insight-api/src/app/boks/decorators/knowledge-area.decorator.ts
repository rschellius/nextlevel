import { createParamDecorator, ExecutionContext } from '@nestjs/common'
import { KnowledgeAreaEntity } from '../entity/knowledge-area.entity'

export const KnowledgeAreaDecorator = createParamDecorator(
  (data: unknown, ctx: ExecutionContext): KnowledgeAreaEntity => {
    const request = ctx.switchToHttp().getRequest()
    return request.KnowledgeArea
  }
)
