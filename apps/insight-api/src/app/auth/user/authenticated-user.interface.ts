// import { UserProperties } from "./user-properties.interface";
import { User as UserProperties } from '@nextlevel/domain'

export interface AuthenticatedUser extends UserProperties {
  token: string
}
