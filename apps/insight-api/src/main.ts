/**
 * This is not a production server yet!
 * This is only a minimal backend to get started.
 */

import { Logger } from '@nestjs/common'
import { NestFactory } from '@nestjs/core'
import { AppModule } from './app/app.module'
import { Neo4jErrorFilter } from 'nest-neo4j/dist'

import * as helmet from 'helmet'

async function bootstrap() {
  const app = await NestFactory.create(AppModule)

  // Set a global filter for handling exceptions
  // You can add your own handlers here
  // https://docs.nestjs.com/exception-filters#binding-filters
  app.useGlobalFilters(new Neo4jErrorFilter())

  const globalPrefix = 'api'
  app.setGlobalPrefix(globalPrefix)

  app.use(helmet())
  app.enableCors({
    origin: ['http://localhost:4200'],
    methods: 'GET,HEAD,PUT,PATCH,POST,DELETE',
    preflightContinue: false,
    optionsSuccessStatus: 204
  })

  const port = process.env.PORT || 3333
  await app.listen(port, () => {
    Logger.log('Listening at this host at port ' + port + '/' + globalPrefix, 'Main')
  })
}

bootstrap()
