// MATCH (n)
// DETACH DELETE n

CREATE(aei: Academy {name: "Academie voor Engineering & ICT", shortName: 'AE&I'})

CREATE(informatica: Education {name: "Informatica", shortName: 'I'})
CREATE (informatica)-[:PART_OF {roles:["maakt deel uit van"], createdAt: datetime()}]->(aei)

CREATE(ruud: Person {name: "Ruud", lastName: 'Hermans'})
CREATE(erco: Person {name: "Erco", lastName: 'Argante'})
CREATE(jan: Person {name: "Jan", lastName: 'Montizaan'})
CREATE(gitta: Person {name: "Gitta", lastName: 'de Vaan'})
CREATE(arno: Person {name: "Arno", lastName: 'Broeders'})
CREATE(pascal: Person {name: "Pascal", lastName: 'van Gastel'})
CREATE(quratulain: Person {name: "Quratulain", lastName: 'Mubarak'})
CREATE(gerard: Person {name: "Gerard", lastName: 'Wagenaar'})
CREATE(ger: Person {name: "Ger", lastName: 'Oosting'})
CREATE(evertjan: Person {name: "Evert Jan", lastName: 'de Voogt'})
CREATE(peter: Person {name: "Peter", lastName: 'Vos'})
CREATE(dion: Person {name: "Dion", lastName: 'Koeze'})
CREATE(alexander: Person {name: "Alexander", lastName: 'van den Bulck'})
CREATE(erik: Person {name: "Erik", lastName: 'Kuiper'})
CREATE(heidi: Person {name: "Heidie", lastName: 'Tops'})
CREATE(eefje: Person {name: "Eefje", lastName: 'Gijzen'})
CREATE(johan: Person {name: "Johan", lastName: 'Smarius'})
CREATE(robin: Person {name: "Robin", lastName: 'Schellius'})

CREATE
	(ruud)-[:WORKS_AT {roles:["Docent"], year: [2018, 2019]}]->(informatica),
	(erco)-[:WORKS_AT {roles:["Docent"], year: [2018, 2019]}]->(informatica),
	(jan)-[:WORKS_AT {roles:["Docent"], year: [2018, 2019]}]->(informatica),
	(gitta)-[:WORKS_AT {roles:["Docent"], year: [2018, 2019]}]->(informatica),
	(arno)-[:WORKS_AT {roles:["Docent"], year: [2018, 2019]}]->(informatica),
	(pascal)-[:WORKS_AT {roles:["Docent"], year: [2018, 2019]}]->(informatica),
	(quratulain)-[:WORKS_AT {roles:["Docent"], year: [2018, 2019]}]->(informatica),
	(gerard)-[:WORKS_AT {roles:["Docent"], year: [2018, 2019]}]->(informatica),
	(ger)-[:WORKS_AT {roles:["Docent"], year: [2018, 2019]}]->(informatica),
	(evertjan)-[:WORKS_AT {roles:["Docent"], year: [2018, 2019]}]->(informatica),
	(peter)-[:WORKS_AT {roles:["Docent"], year: [2018, 2019]}]->(informatica),
	(erik)-[:WORKS_AT {roles:["Docent"], year: [2018, 2019]}]->(informatica),
	(dion)-[:WORKS_AT {roles:["Docent"], year: [2018, 2019]}]->(informatica),
	(alexander)-[:WORKS_AT {roles:["Docent"], year: [2018, 2019]}]->(informatica),
	(heidi)-[:WORKS_AT {roles:["Docent"], year: [2018, 2019]}]->(informatica),
	(eefje)-[:WORKS_AT {roles:["Docent"], year: [2018, 2019]}]->(informatica),
	(johan)-[:WORKS_AT {roles:["Docent"], year: [2018, 2019]}]->(informatica),
	(robin)-[:WORKS_AT {roles:["Docent"], year: [2018, 2019]}]->(informatica)


CREATE
	(jaar1:Year {
		name: 'Jaar 1',
		code: 1,
		shortDescription: 'In het eerste jaar leer je de basis, met de focus op technische vaardigheden.'
		}),
		(jaar1)-[:CREATED_BY {createdAt: datetime()}]->(robin),

	(jaar2:Year {
		name: 'Jaar 2',
		code: 2,
		shortDescription: 'Hier de beschrijving.'
		}),
		(jaar2)-[:CREATED_BY {createdAt: datetime()}]->(robin),

	(jaar3:Year {
		name: 'Jaar 3',
		code: 3,
		shortDescription: 'Hier de beschrijving.'
		}),
		(jaar3)-[:CREATED_BY {createdAt: datetime()}]->(robin),

	(jaar4:Year {
		name: 'Jaar 4',
		code: 4,
		shortDescription: 'Hier de beschrijving.'
		}),
		(jaar4)-[:CREATED_BY {createdAt: datetime()}]->(robin)

CREATE
	(periode1:Period {
		name: 'Periode 1.1',
		code: '1.1',
		shortDescription: 'Hier de beschrijving.'
		}),
		// (periode1)-[:CREATED_BY {createdAt: datetime()}]->(robin),
		(periode1)-[:PART_OF {createdAt: datetime()}]->(jaar1),

	(periode2:Period {
		name: 'Periode 1.2',
		code: '1.2',
		shortDescription: 'Hier de beschrijving.'
		}),
		// (periode2)-[:CREATED_BY {createdAt: datetime()}]->(robin),
		(periode2)-[:PART_OF {createdAt: datetime()}]->(jaar1),

	(periode3:Period {
		name: 'Periode 1.3',
		code: '1.3',
		shortDescription: 'Hier de beschrijving.'
		}),
		// (periode3)-[:CREATED_BY {createdAt: datetime()}]->(robin),
		(periode3)-[:PART_OF {createdAt: datetime()}]->(jaar1),

	(periode4:Period {
		name: 'Periode 1.4',
		code: '1.4',
		shortDescription: 'Hier de beschrijving.'
		}),
		// (periode4)-[:CREATED_BY {createdAt: datetime()}]->(robin),
		(periode4)-[:PART_OF {createdAt: datetime()}]->(jaar1)


CREATE
	(basisvaardigheden1:Course {
		validFrom: datetime('2018-09-01'),
		validUntil: datetime('2019-08-31'),
		name: 'Basisvaardigheden 1',
		shortName: 'I-1.1',
		cohort: 2019,
		form: 'Regulier',
		shortDescription: 'Dit is de eerste periode van jaar 1.',
		longDescription: 'Welkom! Je hebt gekozen voor de opleiding Informatica aan de Academie voor Engineering & ICT (AE&I) van Avans. Dit is de periodewijzer van ‘Periode 1.1: Basisvaardigheden 1’. Iedere periode heeft een periodewijzer waarin relevante informatie over de belangrijkste onderwijsactiviteiten staat.Naast de periodewijzer wordt er ook andere informatie op BlackBoard geplaatst. In deze periode krijg je basisvaardigheden van het vakgebied Informatica aangeleerd.Deze vaardigheden vormen het fundament waarop we in volgende perioden voortbouwen.Het volgen van lessen, en vooral het oefenen door het maken van opdrachten, zijn noodzakelijk om succesvol de opleiding te volgen.'
	}),
	(basisvaardigheden1)-[:OWNER {roles:["Periode eigenaar"], year: [2018, 2019]}]->(ruud),
	(basisvaardigheden1)-[:CREATED_BY {createdAt: datetime()}]->(ruud),
	(basisvaardigheden1)-[:PART_OF {createdAt: datetime()}]->(periode1),
	(basisvaardigheden1)-[:PROVIDED_BY {roles:["wordt gegeven door"], createdAt: datetime()}]->(informatica),

	(basisvaardigheden2:Course {
		validFrom: datetime('2018-09-01'),
		validUntil: datetime('2019-08-31'),
		name: 'Basisvaardigheden 2',
		shortName: 'I-1.2',
		cohort: 2019,
		form: 'Regulier',
		shortDescription: 'Dit is de tweede periode van jaar 1.',
		longDescription: 'Volgt nog.'
	}),
	(basisvaardigheden2)-[:OWNER {roles:["Periode eigenaar"], year: [2018, 2019]}]->(ruud),
	(basisvaardigheden2)-[:CREATED_BY {createdAt: datetime()}]->(ruud),
	(basisvaardigheden2)-[:PART_OF {createdAt: datetime()}]->(periode2),
	(basisvaardigheden2)-[:PROVIDED_BY {roles:["wordt gegeven door"], createdAt: datetime()}]->(informatica)


CREATE
	(EIIN_STVH:OsirisExam {
        osirisCode: 'EIIN_STVH',
        name: 'Studievaardigheden',
        examType: ['Reflectieverslag'],
        credits: 1.0,
        resultScale: 'Voldaan/Niet voldaan',
        minimumRequired: 'Voldaan',
        weightinPercentage: 100,    // Max 100 %
        mutations: 'Vrijstelling voor verkorte traject.',
        lengthInMinutes: 100,
        language: ['Nederlands']
	}),
	(EIIN_STVH)-[:CONTACT_PERSON {roles:["contactperson"], year: [2018, 2019]}]->(evertjan),
	(EIIN_STVH)-[:CREATED_BY {createdAt: datetime()}]->(evertjan),

	(EIIN_RELDAT1:OsirisExam {
        osirisCode: 'EIIN_RELDAT1',
        name: 'Relationele Databases 1',
        examType: ['Vaardigheden', 'Kennis'],
        credits: 3.0,
        resultScale: 'Numeriek',
        minimumRequired: '5,5',
        weightinPercentage: 100,    // Max 100 %
        mutations: 'Vrijstelling voor verkorte traject.',
        lengthInMinutes: 150,
        language: ['Nederlands', 'Engels']
	}),
	(EIIN_RELDAT1)-[:CONTACT_PERSON {roles:["contactperson"], year: [2018, 2019]}]->(alexander),
	(EIIN_RELDAT1)-[:CREATED_BY {createdAt: datetime()}]->(alexander),

	(EIIN_PROGR1:OsirisExam {
        osirisCode: 'EIIN_PROGR1',
        name: 'Programmeren 1',
        examType: ['Vaardigheid', 'Kennis'],
        credits: 4.0,
        resultScale: 'numeriek',
        minimumRequired: '5,5',
        weightinPercentage: 100,    // Max 100 %
        mutations: 'Vrijstelling voor verkorte traject.',
        lengthInMinutes: 100,
        language: ['Engels', 'Nederlands']
	}),
	(EIIN_PROGR1)-[:CONTACT_PERSON {roles:["contactperson"], year: [2018, 2019]}]->(arno),
	(EIIN_PROGR1)-[:CREATED_BY {createdAt: datetime()}]->(ruud)


CREATE
	(ecfAreaPlan:EcfCompetenceArea {
		name: 'Plan',
		code: 'A',
		shortDescription: 'Hier meer info over dit element.'
	}),
	(ecfAreaBuild:EcfCompetenceArea {
		name: 'Build',
		code: 'B',
		shortDescription: 'Hier meer info over dit element.'
	}),
	(ecfAreaRun:EcfCompetenceArea {
		name: 'Run',
		code: 'C',
		shortDescription: 'Hier meer info over dit element.'
	}),
	(ecfAreaEnable:EcfCompetenceArea {
		name: 'Enable',
		code: 'D',
		shortDescription: 'Hier meer info over dit element.'
	}),
	(ecfAreaManage:EcfCompetenceArea {
		name: 'Manage',
		code: 'E',
		shortDescription: 'Hier meer info over dit element.'
	})

CREATE
	(ecfA5:EcfCompetence {
		code: 'A5',
		name: 'Architecture Design',
		shortDescription: 'Hier meer info over dit element.'
	}),
	(ecfA5)-[:IS_PART_OF]->(ecfAreaPlan),

	(ecfA6:EcfCompetence {
		code: 'A6',
		name: 'Application Design',
		shortDescription: 'Hier meer info over dit element.'
	}),
	(ecfA6)-[:IS_PART_OF]->(ecfAreaPlan),

	(ecfA8:EcfCompetence {
		code: 'A8',
		name: 'Sustainable Development',
		shortDescription: 'Hier meer info over dit element.'
	}),
	(ecfA8)-[:IS_PART_OF]->(ecfAreaPlan),

	(ecfB1:EcfCompetence {
		code: 'B1',
		name: 'Application Development',
		category: 'Build',
		shortDescription: 'Hier meer info over dit element.'
	}),
	(ecfB1)-[:IS_PART_OF]->(ecfAreaBuild),

	(ecfB2:EcfCompetence {
		code: 'B2',
		name: 'Component Integration',
		shortDescription: 'Hier meer info over dit element.'
	}),
	(ecfB2)-[:IS_PART_OF]->(ecfAreaBuild),

	(ecfB3:EcfCompetence {
		code: 'B3',
		name: 'Testing',
		shortDescription: 'Hier meer info over dit element.'
	}),
	(ecfB3)-[:IS_PART_OF]->(ecfAreaBuild),

	(ecfB4:EcfCompetence {
		code: 'B4',
		name: 'Solution Deployment',
		shortDescription: 'Hier meer info over dit element.'
	}),
	(ecfB4)-[:IS_PART_OF]->(ecfAreaBuild)

CREATE
	(bloomRemember:BloomLevel {
		order: 1,
		name: 'Remember',
		shortDescription: 'Hier meer info over dit element.'
	}),
	(bloomUnderstand:BloomLevel {
		order: 2,
		name: 'Understand',
		shortDescription: 'Hier meer info over dit element.'
	}),
	(bloomApply:BloomLevel {
		order: 3,
		name: 'Apply',
		shortDescription: 'Hier meer info over dit element.'
	}),
	(bloomAnalyze:BloomLevel {
		order: 4,
		name: 'Analyze',
		shortDescription: 'Hier meer info over dit element.'
	}),
	(bloomEvaluate:BloomLevel {
		order: 5,
		name: 'Evaluate',
		shortDescription: 'Hier meer info over dit element.'
	}),
	(bloomCreate:BloomLevel {
		order: 6,
		name: 'Create',
		shortDescription: 'Hier meer info over dit element.'
	})



CREATE
	(leerdoel_1:Objective {
		name: 'Hier een leerdoel, waarin de student een actie onderneemt, zodanig dat de student iets leert'
	}),
	(leerdoel_1)-[:REALIZES_COMPETENCE {proficiencyLevel: 1, createdAt: datetime()}]->(ecfA5),
	(leerdoel_1)-[:ON_BLOOM_LEVEL {createdAt: datetime()}]->(bloomRemember),

	(leerdoel_2:Objective {
		name: 'De student begrijpt vanuit conceptueel oogpunt wat een computer is, zodanig dat hij de begrippen processor, programmeeromgeving, besturingssyteem en geheugen kan plaatsen.'
	}),
	(leerdoel_2)-[:REALIZES_COMPETENCE {proficiencyLevel: 1, createdAt: datetime()}]->(ecfA5),
	(leerdoel_2)-[:ON_BLOOM_LEVEL {createdAt: datetime()}]->(bloomUnderstand),

	(leerdoel_3:Objective {
		name: 'De student kan in een programmeertaal de concepten control flow en datatypes toepassen, zodanig dat hij een applicatie kan maken.'
	}),
	(leerdoel_3)-[:REALIZES_COMPETENCE {proficiencyLevel: 1, createdAt: datetime()}]->(ecfB1),
	(leerdoel_3)-[:ON_BLOOM_LEVEL {createdAt: datetime()}]->(bloomUnderstand),

	(leerdoel_4:Objective {
		name: 'De student kan in een programmeertaal een console applicatie maken, zodanig dat hij een toepassing maakt die door invoer en uitvoer bediend kan worden.'
	}),
	(leerdoel_4)-[:REALIZES_COMPETENCE {proficiencyLevel: 1, createdAt: datetime()}]->(ecfA5),
	(leerdoel_4)-[:ON_BLOOM_LEVEL {createdAt: datetime()}]->(bloomCreate),

	(leerdoel_5:Objective {
		name: 'De student kan in een programmeeromgeving gebruik maken van een debugger, zodanig dat hij inzicht verwerft in de onderliggende werking van zijn applicatie en fouten uit de programmeercode kan verwijderen.'
	}),
	(leerdoel_5)-[:REALIZES_COMPETENCE {proficiencyLevel: 1, createdAt: datetime()}]->(ecfB1),
	(leerdoel_5)-[:ON_BLOOM_LEVEL {createdAt: datetime()}]->(bloomApply),

	(leerdoel_6:Objective {
		name: 'De student kent de concepten met betrekking tot primitieve datatypen, zodanig dat hij deze op de juiste manier en in de juiste context kan toepassen.'
	}),
	(leerdoel_6)-[:REALIZES_COMPETENCE {proficiencyLevel: 1, createdAt: datetime()}]->(ecfB1),
	(leerdoel_6)-[:ON_BLOOM_LEVEL {createdAt: datetime()}]->(bloomUnderstand),

	(leerdoel_7:Objective {
		name: 'De student gebruikt een programmeeromgeving (IDE), zodanig dat hij een applicatie kan maken, kan debuggen en kan uitvoeren'
	}),
	(leerdoel_7)-[:REALIZES_COMPETENCE {proficiencyLevel: 1, createdAt: datetime()}]->(ecfB1),
	(leerdoel_7)-[:ON_BLOOM_LEVEL {createdAt: datetime()}]->(bloomApply),

	(leerdoel_8:Objective {
		name: 'De student past versie beheer toe op zijn applicatie, zodanig dat hij versies van zijn programmeercode bijhoudt en op professionele wijze kan samenwerken bij het realiseren van een applicatie.'
	}),
	(leerdoel_8)-[:REALIZES_COMPETENCE {proficiencyLevel: 1, createdAt: datetime()}]->(ecfA6),
	(leerdoel_8)-[:ON_BLOOM_LEVEL {createdAt: datetime()}]->(bloomApply),

	(leerdoel_9:Objective {
		name: 'De student kan zijn taken zo inrichten dat hij zijn HBO diploma behaalt.'
	}),
	(leerdoel_9)-[:REALIZES_COMPETENCE {proficiencyLevel: 1, createdAt: datetime()}]->(ecfA6),
	(leerdoel_9)-[:ON_BLOOM_LEVEL {createdAt: datetime()}]->(bloomApply)

//
// Vakken/modules/examenonderdeel
//
//
CREATE
	(studievaardigheden: Module {
		validFrom: datetime('2018-09-01'),
		validUntil: datetime('2019-08-31'),
		name: 'Studievaardigheden',
		shortName: 'STVH',
		form: 'Regulier',
		shortDescription: 'Hier een korte beschrijving.',
		longDescription: 'Hier een lange beschrijving. ',
		firstAttempt: 'Hier de beschrijving van de toetsing. Dit is de reguliere kans, poging 1.',
		secondAttempt: 'Hier de beschrijving van de toetsing. Dit is de reguliere kans, poging 1.',
		studyYear: 1,
		cohort: 2018
	}),
	(studievaardigheden)-[:CONTACT_PERSON {roles:["contactperson"], year: [2018, 2019]}]->(evertjan),
	(studievaardigheden)-[:CREATED_BY {createdAt: datetime()}]->(evertjan),
	(studievaardigheden)-[:UPDATED_BY {createdAt: datetime()}]->(gitta),
	(studievaardigheden)-[:UPDATED_BY {createdAt: datetime()}]->(gitta),
	(studievaardigheden)-[:OSIRIS_EXAM {createdAt: datetime()}]->(EIIN_STVH),
	(studievaardigheden)-[:REALIZES_OBJECTIVE {createdAt: datetime()}]->(leerdoel_9),
	(studievaardigheden)-[:IS_PART_OF {createdAt: datetime()}]->(basisvaardigheden1)

CREATE
	(programmeren1: Module {
		validFrom: datetime('2018-09-01'),
		validUntil: datetime('2019-08-31'),
		name: 'Programmeren 1',
		shortName: 'PROGR1',
		form: 'Regulier',
		shortDescription: 'Hier een korte beschrijving.',
		longDescription: 'Hier een lange beschrijving. ',
		firstAttempt: 'Hier de beschrijving van de toetsing. Dit is de reguliere kans, poging 1.',
		secondAttempt: 'Hier de beschrijving van de toetsing. Dit is de reguliere kans, poging 1.',
		studyYear: 1,
		cohort: 2018
	}),
	(programmeren1)-[:CONTACT_PERSON {roles:["contactperson"], year: [2018, 2019]}]->(arno),
	(programmeren1)-[:CREATED_BY {createdAt: datetime()}]->(pascal),
	(programmeren1)-[:UPDATED_BY {createdAt: datetime()}]->(pascal),
	(programmeren1)-[:UPDATED_BY {createdAt: datetime()}]->(ruud),
	(programmeren1)-[:OSIRIS_EXAM {createdAt: datetime()}]->(EIIN_PROGR1),
	(programmeren1)-[:REALIZES_OBJECTIVE {createdAt: datetime()}]->(leerdoel_2),
	(programmeren1)-[:REALIZES_OBJECTIVE {createdAt: datetime()}]->(leerdoel_3),
	(programmeren1)-[:REALIZES_OBJECTIVE {createdAt: datetime()}]->(leerdoel_4),
	(programmeren1)-[:REALIZES_OBJECTIVE {createdAt: datetime()}]->(leerdoel_5),
	(programmeren1)-[:REALIZES_OBJECTIVE {createdAt: datetime()}]->(leerdoel_6),
	(programmeren1)-[:REALIZES_OBJECTIVE {createdAt: datetime()}]->(leerdoel_7),
	(programmeren1)-[:IS_PART_OF {createdAt: datetime()}]->(basisvaardigheden1)

CREATE
	(programmeren2: Module {
		validFrom: datetime('2018-09-01'),
		validUntil: datetime('2019-08-31'),
		name: 'Programmeren 2',
		shortName: 'PROGR2',
		form: 'Regulier',
		shortDescription: 'Hier een korte beschrijving.',
		longDescription: 'Hier een lange beschrijving. ',
		firstAttempt: 'Hier de beschrijving van de toetsing. Dit is de reguliere kans, poging 1.',
		secondAttempt: 'Hier de beschrijving van de toetsing. Dit is de reguliere kans, poging 1.',
		cohort: 2018,
		studyYear: 1
	}),
	(programmeren2)-[:CONTACT_PERSON {roles:["contactperson"], year: [2018, 2019]}]->(arno),
	(programmeren2)-[:CREATED_BY {createdAt: datetime()}]->(pascal),
	(programmeren2)-[:UPDATED_BY {createdAt: datetime()}]->(pascal),
	(programmeren2)-[:UPDATED_BY {createdAt: datetime()}]->(ruud),
	(programmeren2)-[:OSIRIS_EXAM {createdAt: datetime()}]->(EIIN_PROGR1),
	(programmeren2)-[:REALIZES_OBJECTIVE {createdAt: datetime()}]->(leerdoel_2),
	(programmeren2)-[:REALIZES_OBJECTIVE {createdAt: datetime()}]->(leerdoel_3),
	(programmeren2)-[:IS_PART_OF {createdAt: datetime()}]->(basisvaardigheden1)

CREATE
	(relationeledatabases1: Module {
		validFrom: datetime('2018-09-01'),
		validUntil: datetime('2019-08-31'),
		name: 'Relationele Databases 1',
		shortName: 'RELDAT1',
		form: 'Regulier',
		shortDescription: 'Hier een korte beschrijving.',
		longDescription: 'Hier een lange beschrijving. ',
		firstAttempt: 'Hier de beschrijving van de toetsing. Dit is de reguliere kans, poging 1.',
		secondAttempt: 'Hier de beschrijving van de toetsing. Dit is de reguliere kans, poging 1.',
		cohort: 2018,
		studyYear: 1
	}),
	(relationeledatabases1)-[:CONTACT_PERSON {roles:["contactperson"], year: [2018, 2019]}]->(erik),
	(relationeledatabases1)-[:CREATED_BY {createdAt: datetime()}]->(quratulain),
	(relationeledatabases1)-[:OSIRIS_EXAM {createdAt: datetime()}]->(EIIN_RELDAT1),
	(relationeledatabases1)-[:REALIZES_OBJECTIVE {proficiencyLevel: 2, createdAt: datetime()}]->(leerdoel_1),
	(relationeledatabases1)-[:IS_PART_OF {createdAt: datetime()}]->(basisvaardigheden1)

CREATE
	(relationeledatabases2: Module {
		validFrom: datetime('2018-09-01'),
		validUntil: datetime('2019-08-31'),
		name: 'Relationele Databases 2',
		shortName: 'RELDAT2',
		form: 'Regulier',
		cohort: 2018,
		studyYear: 1,
		shortDescription: 'Hier een korte beschrijving.',
		longDescription: 'Hier een lange beschrijving. ',
		firstAttempt: 'Hier de beschrijving van de toetsing. Dit is de reguliere kans, poging 1.',
		secondAttempt: 'Hier de beschrijving van de toetsing. Dit is de reguliere kans, poging 1.'
	}),
	(relationeledatabases2)-[:CONTACT_PERSON {roles:["contactperson"], year: [2018, 2019]}]->(erik),
	(relationeledatabases2)-[:CREATED_BY {createdAt: datetime()}]->(quratulain),
	(relationeledatabases2)-[:OSIRIS_EXAM {createdAt: datetime()}]->(EIIN_RELDAT1),
	(relationeledatabases2)-[:REALIZES_OBJECTIVE {proficiencyLevel: 2, createdAt: datetime()}]->(leerdoel_1),
	(relationeledatabases2)-[:IS_PART_OF {createdAt: datetime()}]->(basisvaardigheden1)

CREATE
	(relationeledatabases3: Module {
		validFrom: datetime('2018-09-01'),
		validUntil: datetime('2019-08-31'),
		name: 'Relationele Databases 3',
		shortName: 'RELDAT3',
		form: 'Regulier',
		shortDescription: 'Hier een korte beschrijving.',
		longDescription: 'Hier een lange beschrijving. ',
		cohort: 2018,
		studyYear: 1,
		firstAttempt: 'Hier de beschrijving van de toetsing. Dit is de reguliere kans, poging 1.',
		secondAttempt: 'Hier de beschrijving van de toetsing. Dit is de reguliere kans, poging 1.'
	}),
	(relationeledatabases3)-[:CONTACT_PERSON {roles:["contactperson"], year: [2018, 2019]}]->(erik),
	(relationeledatabases3)-[:CREATED_BY {createdAt: datetime()}]->(quratulain),
	(relationeledatabases3)-[:OSIRIS_EXAM {createdAt: datetime()}]->(EIIN_RELDAT1),
	(relationeledatabases3)-[:REALIZES_OBJECTIVE {proficiencyLevel: 2, createdAt: datetime()}]->(leerdoel_1),
	(relationeledatabases3)-[:IS_PART_OF {createdAt: datetime()}]->(basisvaardigheden1)

//
// Kennisgebieden
//
//
CREATE
	(kaProgrammeren:KnowledgeArea {
		name: 'Programmeren',
		shortDescription: 'Hier meer info over het kennisgebied Programmeren.'
	}),
	(kaProgrammeren)-[:CREATED_BY {createdAt: datetime()}]->(robin),
	(kaProgrammeren)-[:CONTACT_PERSON {roles:["contactperson"], createdAt: datetime()}]->(arno),

	(programmeren1)-[:IN_KNOWLEDGE_AREA {createdAt: datetime()}]->(kaProgrammeren),
	(kennisitemOOP:KnowledgeItem {
		name: 'OO programmeren',
		shortDescription: 'Hier meer detail over dit item.'
	})-[:IN_KNOWLEDGE_AREA {createdAt: datetime()}]->(kaProgrammeren),
	(kennisitemOOP)-[:IN_KNOWLEDGE_AREA {createdAt: datetime()}]->(kaProgrammeren),
	(kennisitemOOP)-[:REALIZED_IN {createdAt: datetime()}]->(programmeren1),

	(programmeren2)-[:IN_KNOWLEDGE_AREA {createdAt: datetime()}]->(kaProgrammeren),

//
//
//
	(kaDatabases:KnowledgeArea {
		name: 'Databases',
		shortDescription: 'Hier meer info over het kennisgebied Databases.'
	}),
	(kaDatabases)-[:CREATED_BY {createdAt: datetime()}]->(erik),
	(kaDatabases)-[:CONTACT_PERSON {roles:["contactperson"], createdAt: datetime()}]->(alexander),
	(relationeledatabases1)-[:IN_KNOWLEDGE_AREA {createdAt: datetime()}]->(kaDatabases),
	(relationeledatabases2)-[:IN_KNOWLEDGE_AREA {createdAt: datetime()}]->(kaDatabases),
	(relationeledatabases3)-[:IN_KNOWLEDGE_AREA {createdAt: datetime()}]->(kaDatabases),

//
//
//
	(kaComputernetwerken:KnowledgeArea {
		name: 'Computernetwerken',
		shortDescription: 'Hier meer info over het kennisgebied Computernetwerken.'
	}),
	(kaComputernetwerken)-[:CREATED_BY {createdAt: datetime()}]->(evertjan),
	(kaComputernetwerken)-[:CONTACT_PERSON {roles:["contactperson"], createdAt: datetime()}]->(arno),

//
//
//
	(kaDatascienceAI:KnowledgeArea {
		name: 'Datascience & AI',
		shortDescription: 'Hier meer info over het kennisgebied Datascience & AI.'
	}),
	(kaDatascienceAI)-[:CREATED_BY {createdAt: datetime()}]->(erco),
	(kaDatascienceAI)-[:CONTACT_PERSON {roles:["contactperson"], createdAt: datetime()}]->(erco)

;
