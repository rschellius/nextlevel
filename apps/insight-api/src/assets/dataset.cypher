// MATCH (n)
// DETACH DELETE n

CREATE(ruud: Person {name: "Ruud", lastName: 'Hermans'})
CREATE(erco: Person {name: "Erco", lastName: 'Argante'})
CREATE(jan: Person {name: "Jan", lastName: 'Montizaan'})
CREATE(gitta: Person {name: "Gitta", lastName: 'de Vaan'})
CREATE(arno: Person {name: "Arno", lastName: 'Broeders'})
CREATE(pascal: Person {name: "Pascal", lastName: 'van Gastel'})
CREATE(quratulain: Person {name: "Quratulain", lastName: 'Mubarak'})
CREATE(gerard: Person {name: "Gerard", lastName: 'Wagenaar'})
CREATE(ger: Person {name: "Ger", lastName: 'Oosting'})
CREATE(evertjan: Person {name: "Evert Jan", lastName: 'de Voogt'})
CREATE(peter: Person {name: "Peter", lastName: 'Vos'})
CREATE(dion: Person {name: "Dion", lastName: 'Koeze'})
CREATE(alexander: Person {name: "Alexander", lastName: 'van den Bulck'})
CREATE(erik: Person {name: "Erik", lastName: 'Kuiper'})
CREATE(heidi: Person {name: "Heidie", lastName: 'Tops'})
CREATE(eefje: Person {name: "Eefje", lastName: 'Gijzen'})
CREATE(johan: Person {name: "Johan", lastName: 'Smarius'})
CREATE(robin: Person {name: "Robin", lastName: 'Schellius'})

CREATE
	(jaar1:Year {	name: 'Jaar 1',	code: 1	}),
	(jaar2:Year {	name: 'Jaar 2',	code: 2	}),
	(jaar3:Year {	name: 'Jaar 3',	code: 3	}),
	(jaar4:Year {	name: 'Jaar 4',	code: 4	})

CREATE
	(basisvaardigheden1:Course {
		name: '1.1',
		descriptiveName: 'Basisvaardigheden 1',
		cohort: 2019,
		form: 'Regulier',
		description: 'Welkom! Je hebt gekozen voor de opleiding Informatica aan de Academie voor Engineering & ICT (AE&I) van Avans. Dit is de periodewijzer van ‘Periode 1.1: Basisvaardigheden 1’. Iedere periode heeft een periodewijzer waarin relevante informatie over de belangrijkste onderwijsactiviteiten staat.Naast de periodewijzer wordt er ook andere informatie op BlackBoard geplaatst. In deze periode krijg je basisvaardigheden van het vakgebied Informatica aangeleerd.Deze vaardigheden vormen het fundament waarop we in volgende perioden voortbouwen.Het volgen van lessen, en vooral het oefenen door het maken van opdrachten, zijn noodzakelijk om succesvol de opleiding te volgen.'
	}),
	(basisvaardigheden1)-[:OWNER {roles:["Periode eigenaar"], year: [2018, 2019]}]->(ruud),

	(basisvaardigheden2:Course {
		name: '1.2',
		descriptiveName: 'Basisvaardigheden 2',
		cohort: 2019,
		form: 'Regulier',
		description: 'Volgt nog.'
	}),
	(basisvaardigheden2)-[:OWNER {roles:["Periode eigenaar"], year: [2018, 2019]}]->(ruud)


CREATE
	(ecfAreaPlan:EcfCompetenceArea { name: 'Plan', code: 'A' }),
	(ecfAreaBuild:EcfCompetenceArea { name: 'Build', code: 'B' }),
	(ecfAreaRun:EcfCompetenceArea { name: 'Run', code: 'C' }),
	(ecfAreaEnable:EcfCompetenceArea { name: 'Enable', code: 'D' }),
	(ecfAreaManage:EcfCompetenceArea { name: 'Manage', code: 'E' })

CREATE
	(ecfA5:EcfCompetence { code: 'A5', name: 'Architecture Design' }),
	(ecfA5)-[:PROVIDED_IN]->(ecfAreaPlan),

	(ecfA6:EcfCompetence { code: 'A6', name: 'Application Design' }),
	(ecfA6)-[:PROVIDED_IN]->(ecfAreaPlan),

	(ecfA8:EcfCompetence { code: 'A8', name: 'Sustainable Development' }),
	(ecfA8)-[:PROVIDED_IN]->(ecfAreaPlan),

	(ecfB1:EcfCompetence { code: 'B1', name: 'Application Development' }),
	(ecfB1)-[:PROVIDED_IN]->(ecfAreaBuild),

	(ecfB2:EcfCompetence { code: 'B2', name: 'Component Integration' }),
	(ecfB2)-[:PROVIDED_IN]->(ecfAreaBuild),

	(ecfB3:EcfCompetence { code: 'B3', name: 'Testing' }),
	(ecfB3)-[:PROVIDED_IN]->(ecfAreaBuild),

	(ecfB4:EcfCompetence { code: 'B4', name: 'Solution Deployment' }),
	(ecfB4)-[:PROVIDED_IN]->(ecfAreaBuild)


CREATE
	(leerdoel_1:Objective {
		name: 'Hier een leerdoel, waarin de student een actie onderneemt, zodanig dat de student iets leert'
	}),
	(leerdoel_1)-[:REALIZES {proficiencyLevel: 1, createdAt: datetime()}]->(ecfA5),

	(leerdoel_2:Objective {
		name: 'De student begrijpt vanuit conceptueel oogpunt wat een computer is, zodanig dat hij de begrippen processor, programmeeromgeving, besturingssyteem en geheugen kan plaatsen.'
	}),
	(leerdoel_2)-[:REALIZES {proficiencyLevel: 1, createdAt: datetime()}]->(ecfA5),

	(leerdoel_3:Objective {
		name: 'De student kan in een programmeertaal de concepten control flow en datatypes toepassen, zodanig dat hij een applicatie kan maken.'
	}),
	(leerdoel_3)-[:REALIZES {proficiencyLevel: 1, createdAt: datetime()}]->(ecfB1),

	(leerdoel_4:Objective {
		name: 'De student kan in een programmeertaal een console applicatie maken, zodanig dat hij een toepassing maakt die door invoer en uitvoer bediend kan worden.'
	}),
	(leerdoel_4)-[:REALIZES {proficiencyLevel: 1, createdAt: datetime()}]->(ecfA5),

	(leerdoel_5:Objective {
		name: 'De student kan in een programmeeromgeving gebruik maken van een debugger, zodanig dat hij inzicht verwerft in de onderliggende werking van zijn applicatie en fouten uit de programmeercode kan verwijderen.'
	}),
	(leerdoel_5)-[:REALIZES {proficiencyLevel: 1, createdAt: datetime()}]->(ecfB1),

	(leerdoel_6:Objective {
		name: 'De student kent de concepten met betrekking tot primitieve datatypen, zodanig dat hij deze op de juiste manier en in de juiste context kan toepassen.'
	}),
	(leerdoel_6)-[:REALIZES {proficiencyLevel: 1, createdAt: datetime()}]->(ecfB1),

	(leerdoel_7:Objective {
		name: 'De student gebruikt een programmeeromgeving (IDE), zodanig dat hij een applicatie kan maken, kan debuggen en kan uitvoeren'
	}),
	(leerdoel_7)-[:REALIZES {proficiencyLevel: 1, createdAt: datetime()}]->(ecfB1),

	(leerdoel_8:Objective {
		name: 'De student past versie beheer toe op zijn applicatie, zodanig dat hij versies van zijn programmeercode bijhoudt en op professionele wijze kan samenwerken bij het realiseren van een applicatie.'
	}),
	(leerdoel_8)-[:REALIZES {proficiencyLevel: 1, createdAt: datetime()}]->(ecfA6),

	(leerdoel_9:Objective {
		name: 'De student kan zijn taken zo inrichten dat hij zijn HBO diploma behaalt.'
	}),
	(leerdoel_9)-[:REALIZES {proficiencyLevel: 1, createdAt: datetime()}]->(ecfA6)

//
// Vakken/modules/examenonderdeel
//
//
CREATE
	(studievaardigheden: Module {
		name: 'STVH',
		descriptiveName: 'Studievaardigheden',
		form: 'Regulier',
		description: 'Hier een lange beschrijving. ',
		studyYear: 1,
		cohort: 2018
	}),
	(studievaardigheden)-[:REALIZES ]->(leerdoel_9),
	(studievaardigheden)-[:PROVIDED_IN ]->(basisvaardigheden1)

CREATE
	(programmeren1: Module {
		name: 'PROGR1',
		descriptiveName: 'Programmeren 1',
		form: 'Regulier',
		description: 'Hier een lange beschrijving. ',
		studyYear: 1,
		cohort: 2018
	}),
	(programmeren1)-[:REALIZES ]->(leerdoel_2),
	(programmeren1)-[:REALIZES ]->(leerdoel_3),
	(programmeren1)-[:PROVIDED_IN ]->(basisvaardigheden1)

CREATE
	(programmeren2: Module {
		name: 'PROGR2',
		descriptiveName: 'Programmeren 2',
		form: 'Regulier',
		description: 'Hier een lange beschrijving. ',
		cohort: 2018,
		studyYear: 1
	}),
	(programmeren2)-[:REALIZES ]->(leerdoel_6),
	(programmeren2)-[:REALIZES ]->(leerdoel_7),
	(programmeren2)-[:PROVIDED_IN ]->(basisvaardigheden2)

CREATE
	(relationeledatabases1: Module {
		name: 'RELDAT1',
		descriptiveName: 'Relationele Databases 1',
		form: 'Regulier',
		description: 'Hier een lange beschrijving. ',
		cohort: 2018,
		studyYear: 1
	}),
	(relationeledatabases1)-[:REALIZES {proficiencyLevel: 2, createdAt: datetime()}]->(leerdoel_1),
	(relationeledatabases1)-[:PROVIDED_IN ]->(basisvaardigheden1)

CREATE
	(relationeledatabases2: Module {
		name: 'RELDAT2',
		descriptiveName: 'Relationele Databases 2',
		form: 'Regulier',
		cohort: 2018,
		studyYear: 1,
		description: 'Hier een lange beschrijving. '
	}),
	(relationeledatabases2)-[:REALIZES {proficiencyLevel: 2, createdAt: datetime()}]->(leerdoel_1),
	(relationeledatabases2)-[:PROVIDED_IN ]->(basisvaardigheden2)

//
// Kennisgebieden
//
//
CREATE
	(kaProgrammeren:KnowledgeArea { name: 'Programmeren' }),
	(kaOO:KnowledgeArea { name: 'Imperatief en OO Programmeren' })-[:IN_KNOWLEDGE_AREA ]->(kaProgrammeren),

	(ki1:KnowledgeItem { name: 'IDE' })-[:IN_KNOWLEDGE_AREA ]->(kaOO),
	(ki1)<-[:REALIZES ]-(programmeren1),
	(ki2:KnowledgeItem { name: 'Static methods' })-[:IN_KNOWLEDGE_AREA ]->(kaOO),
	(ki2)<-[:REALIZES ]-(programmeren1),
	(ki3:KnowledgeItem { name: 'Console I/O' })-[:IN_KNOWLEDGE_AREA ]->(kaOO),
	(ki3)<-[:REALIZES ]-(programmeren1),
	(ki4:KnowledgeItem { name: 'Conditionals' })-[:IN_KNOWLEDGE_AREA ]->(kaOO),
	(ki4)<-[:REALIZES ]-(programmeren1),
	(ki5:KnowledgeItem { name: 'Loops' })-[:IN_KNOWLEDGE_AREA ]->(kaOO),
	(ki5)<-[:REALIZES ]-(programmeren1),
	(ki6:KnowledgeItem { name: 'Array(List)' })-[:IN_KNOWLEDGE_AREA ]->(kaOO),
	(ki6)<-[:REALIZES ]-(programmeren1),
	(ki7:KnowledgeItem { name: 'Variables' })-[:IN_KNOWLEDGE_AREA ]->(kaOO),
	(ki7)<-[:REALIZES ]-(programmeren1),
	(ki8:KnowledgeItem { name: 'Classes/Objects' })-[:IN_KNOWLEDGE_AREA ]->(kaOO),
	(ki8)<-[:REALIZES ]-(programmeren1),
	(ki9:KnowledgeItem { name: 'Constructors' })-[:IN_KNOWLEDGE_AREA ]->(kaOO),
	(ki9)<-[:REALIZES ]-(programmeren1),
	(ki10:KnowledgeItem { name: 'Methods' })-[:IN_KNOWLEDGE_AREA ]->(kaOO),
	(ki10)<-[:REALIZES ]-(programmeren1),
	(ki11:KnowledgeItem { name: 'Visibility' })-[:IN_KNOWLEDGE_AREA ]->(kaOO),
	(ki11)<-[:REALIZES ]-(programmeren1),
	(ki12:KnowledgeItem { name: 'Operators' })-[:IN_KNOWLEDGE_AREA ]->(kaOO),
	(ki12)<-[:REALIZES ]-(programmeren1),
	(ki13:KnowledgeItem { name: 'Scope' })-[:IN_KNOWLEDGE_AREA ]->(kaOO),
	(ki13)<-[:REALIZES ]-(programmeren1),
	(ki14:KnowledgeItem { name: 'Overloading' })-[:IN_KNOWLEDGE_AREA ]->(kaOO),
	(ki14)<-[:REALIZES ]-(programmeren1),
	(ki15:KnowledgeItem { name: 'Debugging' })-[:IN_KNOWLEDGE_AREA ]->(kaOO),
	(ki15)<-[:REALIZES ]-(programmeren1),
	(ki16:KnowledgeItem { name: 'Java' })-[:IN_KNOWLEDGE_AREA ]->(kaOO),
	(ki16)<-[:REALIZES ]-(programmeren1),

//
//
//
	(inheritance:KnowledgeItem { name: 'Inheritance' })-[:IN_KNOWLEDGE_AREA ]->(kaOO),
	(inheritance)<-[:REALIZES ]-(programmeren2),
	(polymorphism:KnowledgeItem { name: 'polymorphism' })-[:IN_KNOWLEDGE_AREA ]->(kaOO),
	(polymorphism)<-[:REALIZES ]-(programmeren2),
	(interfaces:KnowledgeItem { name: 'interfaces' })-[:IN_KNOWLEDGE_AREA ]->(kaOO),
	(interfaces)<-[:REALIZES ]-(programmeren2),
	(abstractclasses:KnowledgeItem { name: 'abstract classes' })-[:IN_KNOWLEDGE_AREA ]->(kaOO),
	(abstractclasses)<-[:REALIZES ]-(programmeren2),
	(fileio:KnowledgeItem { name: 'fileio' })-[:IN_KNOWLEDGE_AREA ]->(kaOO),
	(fileio)<-[:REALIZES ]-(programmeren2),
	(gui:KnowledgeItem { name: 'gui' })-[:IN_KNOWLEDGE_AREA ]->(kaOO),
	(gui)<-[:REALIZES ]-(programmeren2),
	(hashmap:KnowledgeItem { name: 'hashmap' })-[:IN_KNOWLEDGE_AREA ]->(kaOO),
	(hashmap)<-[:REALIZES ]-(programmeren2),
	(exceptions:KnowledgeItem { name: 'exceptions' })-[:IN_KNOWLEDGE_AREA ]->(kaOO),
	(exceptions)<-[:REALIZES ]-(programmeren2),
	(enums:KnowledgeItem { name: 'enums' })-[:IN_KNOWLEDGE_AREA ]->(kaOO),
	(enums)<-[:REALIZES ]-(programmeren2),
	(packages:KnowledgeItem { name: 'packages' })-[:IN_KNOWLEDGE_AREA ]->(kaOO),
	(packages)<-[:REALIZES ]-(programmeren2),



	(kaSWE:KnowledgeArea { name: 'Software Engineering' }),
	(kaSCM:KnowledgeArea { name: 'SCM' })-[:IN_KNOWLEDGE_AREA ]->(kaSWE),
	(kaBedrijfsprocessen:KnowledgeArea { name: 'Bedrijfsprocessen' })-[:IN_KNOWLEDGE_AREA ]->(kaSWE),
	(kaRequirementsEngineering:KnowledgeArea { name: 'RequirementsEngineering' })-[:IN_KNOWLEDGE_AREA ]->(kaSWE),
	(kaProjMgmtOntwikkelMethoden:KnowledgeArea { name: 'ProjMgmtOntwikkelMethoden' })-[:IN_KNOWLEDGE_AREA ]->(kaSWE),

	(kiGIT:KnowledgeItem { name: 'GIT' })-[:IN_KNOWLEDGE_AREA ]->(kaSCM),
	(kiGIT)<-[:REALIZES ]-(programmeren2),
	(kiMergeConflicts:KnowledgeItem { name: 'MergeConflicts' })-[:IN_KNOWLEDGE_AREA ]->(kaSCM),
	(kiMergeConflicts)<-[:REALIZES ]-(programmeren2),
	(kiExterneRepo:KnowledgeItem { name: 'ExterneRepo' })-[:IN_KNOWLEDGE_AREA ]->(kaSCM),
	(kiExterneRepo)<-[:REALIZES ]-(programmeren2),
	(kiCommits:KnowledgeItem { name: 'Commits' })-[:IN_KNOWLEDGE_AREA ]->(kaSCM),
	(kiCommits)<-[:REALIZES ]-(programmeren2),

//
//
//
	(kaDatabases:KnowledgeArea { name: 'Databases' }),
	// (relationeledatabases1)-[:IN_KNOWLEDGE_AREA ]->(kaDatabases),
	// (relationeledatabases2)-[:IN_KNOWLEDGE_AREA ]->(kaDatabases),
	// (relationeledatabases3)-[:IN_KNOWLEDGE_AREA ]->(kaDatabases),

//
//
//
	(kaComputernetwerken:KnowledgeArea { name: 'Computernetwerken' }),

//
//
//
	(kaDatascienceAI:KnowledgeArea { name: 'Datascience & AI' })



;
