// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

import { CoreConfig } from '@nextlevel/domain'
import { NgxLoggerLevel } from 'ngx-logger'

export const environment: CoreConfig = {
  production: true,

  API_URL: 'http://localhost:3333/api/',

  socketIoConfig: { url: 'http://localhost:4444', options: {} },

  // Config voor NxgLogger
  logConfig: {
    serverLoggingUrl: 'http://localhost:3000/api/logs',
    level: NgxLoggerLevel.ERROR,
    serverLogLevel: NgxLoggerLevel.OFF,
    disableConsoleLogging: true,
    httpResponseType: 'text' as 'json'
  }
}
