import { TestBed, async, ComponentFixture } from '@angular/core/testing'
import { AdminLayoutComponent } from './admin-layout.component'
import { Component, Directive, HostListener, Input } from '@angular/core'
import { Router } from '@angular/router'
import { AuthModule } from '@nextlevel/auth'

//
// Since the app.component.html template uses some component selectors,
// we need to stub these in the test.
//
@Component({ selector: 'sidebar-cmp', template: '' })
class SidebarStubComponent {}

@Component({ selector: 'navbar-cmp', template: '' })
class NavbarStubComponent {
  @Input() title: string
}

@Component({ selector: 'footer-cmp', template: '' })
class FooterStubComponent {}

@Component({ selector: 'fixedplugin-cmp', template: '' })
class FixedPluginStubComponent {}

// tslint:disable-next-line: component-selector
@Component({ selector: 'router-outlet', template: '' })
class RouterOutletStubComponent {}

@Directive({
  // tslint:disable-next-line: directive-selector
  selector: '[routerLink]'
})
class RouterLinkStubDirective {
  // tslint:disable-next-line: no-input-rename
  @Input('routerLink') linkParams: any
  navigatedTo: any = null

  @HostListener('click')
  onClick() {
    this.navigatedTo = this.linkParams
  }
}

//
// The test suite for AppComponent.
//
describe.skip('AdminLayoutComponent', () => {
  let fixture: ComponentFixture<AdminLayoutComponent>
  let app: AdminLayoutComponent
  let routerSpy
  let authServiceSpy // : { userIsLoggedIn: jasmine.Spy }

  beforeEach(() => {
    //
    //
    // LET OP!
    //
    // Deze test werkt nog niet; jasmine functies nog omzetten naar Jest functies.
    // https://www.xfive.co/blog/testing-angular-faster-jest/
    //
    // routerSpy = jest.fn({key: jest.fn()})
    //
    //
    //
    routerSpy = jasmine.createSpyObj('Router', ['navigateByUrl'])
    authServiceSpy = jasmine.createSpyObj('authService', ['login', 'userIsLoggedIn'])

    TestBed.configureTestingModule({
      declarations: [
        // The 'real' component that we will test
        AdminLayoutComponent,

        // Required stubbed components
        SidebarStubComponent,
        NavbarStubComponent,
        FooterStubComponent,
        FixedPluginStubComponent,
        RouterOutletStubComponent,
        RouterLinkStubDirective
      ],
      imports: [AuthModule],
      // Don't provide the real service! Provide a test-double instead!
      providers: [
        { provide: Router, useValue: routerSpy }
        // { provide: AuthService, useValue: authServiceSpy }
      ]
    }).compileComponents()

    fixture = TestBed.createComponent(AdminLayoutComponent)
    app = fixture.debugElement.componentInstance
  })

  afterEach(() => {
    fixture.destroy()
  })

  it('should create the app', () => {
    fixture.detectChanges()
    expect(app).toBeTruthy()
  })

  it(`should have as title 'courses-customer-portal'`, () => {
    fixture.detectChanges()
    expect(app.title.toLowerCase()).toContain('customer')
  })

  // it('should render title', () => {
  //   fixture = TestBed.createComponent(AppComponent)
  //   fixture.detectChanges()
  //   const compiled = fixture.nativeElement
  //   expect(compiled.querySelector('h1').textContent).toContain('Welcome to courses-customer-portal!')
  // })
})
