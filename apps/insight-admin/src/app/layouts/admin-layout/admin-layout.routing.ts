import { Routes } from '@angular/router'
import { DashboardComponent } from '../../pages/dashboard/dashboard.component'
import { UserComponent } from '../../pages/user/user.component'
import { TableComponent } from '../../pages/table/table.component'

export const AdminLayoutRoutes: Routes = [
  { path: 'dashboard', component: DashboardComponent },
  { path: 'user', component: UserComponent },
  { path: 'table', component: TableComponent },
  {
    path: 'modules',
    loadChildren: () =>
      import(/* webpackChunkName: "modules-module" */ '../../pages/modules/modules.module').then(
        (module) => module.ModulesModule,
        () => {
          throw { loadChunkError: true }
        }
      )
  }
]
