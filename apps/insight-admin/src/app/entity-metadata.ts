import { EntityMetadataMap } from '@ngrx/data'
import { Module } from '@nextlevel/domain'

const entityMetadata: EntityMetadataMap = {
  Module: {
    selectId: (module: Module) => module.identity.low
  }
  // Villain: {}
}

// because the plural of "hero" is not "heros"
// const pluralNames = { Hero: 'Heroes' }

export const entityConfig = {
  entityMetadata
  // pluralNames
}
