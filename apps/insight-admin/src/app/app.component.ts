import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core'
import { AuthenticationState, IsLoggedIn } from '@nextlevel/auth'
import { Store } from '@ngrx/store'

@Component({
  selector: 'insight-root',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './app.component.html'
})
export class AppComponent implements OnInit {
  title = 'insight-admin'

  constructor(private store: Store<AuthenticationState>) {}

  ngOnInit() {
    this.store.dispatch(new IsLoggedIn())
  }
}
