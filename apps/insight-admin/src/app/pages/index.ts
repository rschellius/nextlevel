import { DashboardComponent } from './dashboard/dashboard.component'
import { UserComponent } from './user/user.component'
import { TableComponent } from './table/table.component'
import { LoginComponent } from './auth/login/login.component'
import { RegisterComponent } from './auth/register/register.component'

export const components: any[] = [
  DashboardComponent,
  UserComponent,
  TableComponent,
  LoginComponent,
  RegisterComponent
]

export * from './dashboard/dashboard.component'
export * from './table/table.component'
export * from './user/user.component'
export * from './auth/login/login.component'
export * from './auth/register/register.component'

// export { ModulesModule } from './modules/modules.module'
