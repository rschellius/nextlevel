import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core'

@Component({
  selector: 'insight-modules',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './module-layout.component.html'
})
export class ModuleLayoutComponent implements OnInit {
  constructor() {}

  ngOnInit(): void {}
}
