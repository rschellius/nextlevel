import { RouterModule, Routes } from '@angular/router'
import { IsLoggedInGuard } from '@nextlevel/auth'
import { ModuleLayoutComponent } from './module-layout.component'
import { ModuleListComponent } from './containers/module-list/module-list.component'
import { ModuleDetailComponent } from './containers/module-detail/module-detail.component'

export const routes: Routes = [
  {
    path: '',
    component: ModuleLayoutComponent,
    canActivate: [IsLoggedInGuard],
    children: [
      { path: '', component: ModuleListComponent },
      { path: ':id', component: ModuleDetailComponent }
    ]
  }
]
