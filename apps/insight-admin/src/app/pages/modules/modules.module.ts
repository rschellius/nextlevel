import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { StoreModule } from '@ngrx/store'
import { EffectsModule } from '@ngrx/effects'
import { RouterModule } from '@angular/router'
import { FormsModule, ReactiveFormsModule } from '@angular/forms'
import { routes } from './module.routing'
import * as fromContainers from './containers/'
import { NgbModule } from '@ng-bootstrap/ng-bootstrap'
import { AuthModule } from '@nextlevel/auth'
import { environment } from '../../../environments/environment'
import { HttpClientModule } from '@angular/common/http'
import { ModuleLayoutComponent } from './module-layout.component'
import { AlertModule } from '@nextlevel/alert'

@NgModule({
  declarations: [ModuleLayoutComponent, ...fromContainers.containers],
  imports: [
    CommonModule,
    AlertModule,
    AuthModule.forRoot(environment),
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    HttpClientModule,
    StoreModule.forFeature('modules', {}),
    EffectsModule.forFeature([])
  ]
})
export class ModulesModule {}
