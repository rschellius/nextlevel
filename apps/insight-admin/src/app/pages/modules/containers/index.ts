import { ModuleDetailComponent } from './module-detail/module-detail.component'
import { ModuleListComponent } from './module-list/module-list.component'

export const containers: any[] = [ModuleDetailComponent, ModuleListComponent]

// export * from './module-detail/module-detail.component'
// export * from './module-list/module-list.component'
