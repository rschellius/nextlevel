import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core'
import { Module } from '@nextlevel/domain'
import { Observable } from 'rxjs'
import { ModuleService } from '../../module.service'

@Component({
  selector: 'insight-module-list',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './module-list.component.html'
})
export class ModuleListComponent implements OnInit {
  modules$: Observable<Module[]>

  constructor(private readonly moduleService: ModuleService) {
    this.modules$ = this.moduleService.entities$
  }

  ngOnInit(): void {
    console.log('ModuleListComponent loaded')
    this.moduleService.getAll()
  }
}
