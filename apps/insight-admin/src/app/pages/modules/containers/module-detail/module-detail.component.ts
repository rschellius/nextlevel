import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core'
import { Module } from '@nextlevel/domain'
import { Observable } from 'rxjs'
import { filter, map } from 'rxjs/operators'
import { ModuleService } from '../../module.service'

@Component({
  selector: 'insight-module-detail',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './module-detail.component.html'
})
export class ModuleDetailComponent implements OnInit {
  module$: Observable<Module[]>

  constructor(private readonly moduleService: ModuleService) {}

  ngOnInit(): void {
    this.module$ = this.moduleService.entities$.pipe(
      map((items: Module[]) => items.filter((item) => item.identity.low === 265))
    )
    // this.module$ = this.moduleService.
  }
}
