import { Injectable } from '@angular/core'
import { EntityCollectionServiceBase, EntityCollectionServiceElementsFactory } from '@ngrx/data'
import { Module } from '@nextlevel/domain'

@Injectable({ providedIn: 'root' })
export class ModuleService extends EntityCollectionServiceBase<Module> {
  constructor(serviceElementsFactory: EntityCollectionServiceElementsFactory) {
    super('Module', serviceElementsFactory)
  }
}
