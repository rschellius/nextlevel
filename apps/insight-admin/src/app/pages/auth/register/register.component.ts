import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core'
import { Store } from '@ngrx/store'
import { Authenticate } from '@nextlevel/domain'
import { AuthenticationState, Register } from '@nextlevel/auth'

@Component({
  selector: 'insight-register-container',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  title = 'Register'

  constructor(private store: Store<AuthenticationState>) {}

  ngOnInit() {}

  onSubmit(event: Authenticate) {
    this.store.dispatch(new Register(event))
  }
}
