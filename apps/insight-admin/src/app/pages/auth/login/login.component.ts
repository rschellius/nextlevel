import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core'
import { FormControl, FormGroup, Validators } from '@angular/forms'
import { Store } from '@ngrx/store'
import { Authenticate } from '@nextlevel/domain'
import { AuthenticationState, Login } from '@nextlevel/auth'

@Component({
  selector: 'insight-login-container',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup | undefined
  title = 'Sign In'

  constructor(private store: Store<AuthenticationState>) {}

  ngOnInit() {}

  onSubmit(event: Authenticate) {
    this.store.dispatch(new Login(event))
  }
}
