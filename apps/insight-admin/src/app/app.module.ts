import { BrowserAnimationsModule } from '@angular/platform-browser/animations'
import { NgModule } from '@angular/core'
import { RouterModule } from '@angular/router'
import { ToastrModule } from 'ngx-toastr'

import { AppComponent } from './app.component'
import { AppRoutes } from './app.routing'
import { NxModule } from '@nrwl/angular'
import { AdminLayoutComponent } from './layouts/admin-layout/admin-layout.component'
import { BrowserModule } from '@angular/platform-browser'
import { AuthModule } from '@nextlevel/auth'
import { StoreModule } from '@ngrx/store'
import { EffectsModule } from '@ngrx/effects'
import { StoreDevtoolsModule } from '@ngrx/store-devtools'
import { environment } from '../environments/environment'
import { NgbModule } from '@ng-bootstrap/ng-bootstrap'
import { UserModule } from '@nextlevel/user'
import * as fromShared from './shared'
import { SocketIoModule } from 'ngx-socket-io'
import { DefaultDataServiceConfig, EntityDataModule } from '@ngrx/data'
import { entityConfig } from './entity-metadata'

@NgModule({
  declarations: [AppComponent, AdminLayoutComponent, ...fromShared.components],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    NxModule.forRoot(),
    ToastrModule.forRoot({
      timeOut: 3000,
      closeButton: true,
      enableHtml: true,
      toastClass: 'alert alert-success alert-with-icon',
      positionClass: 'toast-bottom-left'
    }),
    AuthModule.forRoot(environment),
    RouterModule.forRoot(AppRoutes),
    UserModule,
    NgbModule,
    StoreModule.forRoot({}),
    EffectsModule.forRoot([]),
    EntityDataModule.forRoot(entityConfig),
    environment.production ? [] : StoreDevtoolsModule.instrument(),
    SocketIoModule.forRoot(environment.socketIoConfig)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
