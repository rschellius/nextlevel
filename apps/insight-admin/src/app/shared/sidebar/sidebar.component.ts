import { Component, Input, OnInit } from '@angular/core'

export interface RouteInfo {
  path: string
  title: string
  icon: string
  class: string
}

export const ROUTES: RouteInfo[] = [
  { path: '/dashboard', title: 'Dashboard', icon: 'nc-bank', class: '' },
  { path: '/modules', title: 'Modules', icon: 'nc-single-02', class: '' },
  { path: '/user', title: 'User Profile', icon: 'nc-single-02', class: '' },
  { path: '/table', title: 'Table List', icon: 'nc-tile-56', class: '' }
]

@Component({
  selector: 'sidebar-cmp',
  templateUrl: 'sidebar.component.html'
})
export class SidebarComponent implements OnInit {
  public menuItems: any[]
  @Input() title = 'Inzicht'

  ngOnInit() {
    this.menuItems = ROUTES.filter((menuItem) => menuItem)
  }
}
