export const getGreeting = () => cy.get('div > h4')
export const getLogo = () => cy.get('.logo > .logo-normal')
