import { Injectable } from '@nestjs/common'
import { Message } from '@nextlevel/domain'

@Injectable()
export class AppService {
  getData(): Message {
    return { message: 'Welcome to api!' }
  }
}
