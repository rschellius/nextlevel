import { CoreConfig } from '@nextlevel/domain'
import { NgxLoggerLevel } from 'ngx-logger'

export const environment: CoreConfig = {
  production: true,

  API_URL: 'http://localhost:3333/api/',

  // Config voor NxgLogger
  logConfig: {
    serverLoggingUrl: 'http://localhost:3000/api/logs',
    level: NgxLoggerLevel.ERROR,
    serverLogLevel: NgxLoggerLevel.OFF,
    disableConsoleLogging: true,
    httpResponseType: 'text' as 'json'
  }
}
