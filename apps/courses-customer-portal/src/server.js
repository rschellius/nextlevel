const express = require('express')
const path = require('path')
const http = require('http')
const compression = require('compression')
const app = express()
app.use(compression())

const appname = 'courses-customer-portal'
const distpath = path.join(__dirname, '..', '..', '..', 'dist', 'apps', appname)
app.use(express.static(distpath))

app.get('*', (req, res) => {
  res.sendFile(distpath, 'index.html')
})

const port = process.env.PORT || '4200'
app.set('port', port)
const server = http.createServer(app)
server.listen(port, () => {
  console.log(`\'${appname}\' running in ${process.env.NODE_ENV || 'development'} mode on port ${port}`)
})
