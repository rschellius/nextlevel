import { NgModule } from '@angular/core'
import { RouterModule } from '@angular/router'
import { CommonModule } from '@angular/common'
import { FormsModule } from '@angular/forms'
import { AdminLayoutRoutes } from './admin-layout.routing'
import * as fromPages from '../../pages'
import { NgbModule } from '@ng-bootstrap/ng-bootstrap'
import { AuthModule } from '@nextlevel/auth'
import { environment } from '../../../environments/environment'

@NgModule({
  declarations: [...fromPages.components],
  imports: [
    CommonModule,
    AuthModule.forRoot(environment),
    RouterModule.forChild(AdminLayoutRoutes),
    FormsModule,
    NgbModule
  ]
})
export class AdminLayoutModule {}
