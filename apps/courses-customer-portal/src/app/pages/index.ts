import { DashboardComponent } from './dashboard/dashboard.component'
import { UserComponent } from './user/user.component'
import { TableComponent } from './table/table.component'
import { TypographyComponent } from './typography/typography.component'
import { NotificationsComponent } from './notifications/notifications.component'
import { UpgradeComponent } from './upgrade/upgrade.component'
import { LoginComponent } from './auth/login/login.component'
import { RegisterComponent } from './auth/register/register.component'

export const components: any[] = [
  DashboardComponent,
  UserComponent,
  TableComponent,
  TypographyComponent,
  NotificationsComponent,
  UpgradeComponent,
  LoginComponent,
  RegisterComponent
]

export * from './dashboard/dashboard.component'
export * from './notifications/notifications.component'
export * from './table/table.component'
export * from './typography/typography.component'
export * from './upgrade/upgrade.component'
export * from './user/user.component'
export * from './auth/login/login.component'
export * from './auth/register/register.component'
