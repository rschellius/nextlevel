import { Routes } from '@angular/router'
import { AdminLayoutComponent } from './layouts/admin-layout/admin-layout.component'
import { IsLoggedInGuard } from '@nextlevel/auth'
import * as fromPages from './pages'

export const AppRoutes: Routes = [
  {
    path: '',
    redirectTo: 'dashboard',
    pathMatch: 'full'
  },
  {
    path: '',
    component: AdminLayoutComponent,
    canActivate: [IsLoggedInGuard],
    children: [
      {
        path: '',
        canActivate: [IsLoggedInGuard],
        loadChildren: () =>
          import(/* webpackChunkName: "admin-layout" */ './layouts/admin-layout/admin-layout.module').then(
            (module) => module.AdminLayoutModule,
            () => {
              throw { loadChunkError: true }
            }
          )
      }
    ]
  },
  { path: 'login', pathMatch: 'full', component: fromPages.LoginComponent },
  { path: 'register', pathMatch: 'full', component: fromPages.RegisterComponent },
  {
    path: '**',
    redirectTo: 'dashboard'
  }
]
