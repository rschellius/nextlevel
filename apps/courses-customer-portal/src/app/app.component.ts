import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core'
import { Store } from '@ngrx/store'
import { AuthenticationState, IsLoggedIn } from '@nextlevel/auth'

@Component({
  selector: 'custportal-root',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './app.component.html'
})
export class AppComponent implements OnInit {
  title = 'courses-customer-portal'

  constructor(private store: Store<AuthenticationState>) {}

  ngOnInit() {
    this.store.dispatch(new IsLoggedIn())
  }
}
