import { BrowserAnimationsModule } from '@angular/platform-browser/animations'
import { NgModule } from '@angular/core'
import { RouterModule } from '@angular/router'
import { ToastrModule } from 'ngx-toastr'

import { AppComponent } from './app.component'
import { AppRoutes } from './app.routing'
import { NxModule } from '@nrwl/angular'
import { AdminLayoutComponent } from './layouts/admin-layout/admin-layout.component'
import { BrowserModule } from '@angular/platform-browser'
import { AuthModule } from '@nextlevel/auth'
import { StoreModule } from '@ngrx/store'
import { EffectsModule } from '@ngrx/effects'
import { StoreDevtoolsModule } from '@ngrx/store-devtools'
import { environment } from '../environments/environment'
import { NgbModule } from '@ng-bootstrap/ng-bootstrap'
// import { StoreRouterConnectingModule } from '@ngrx/router-store'
import { UserModule } from '@nextlevel/user'

import * as fromShared from './shared'

@NgModule({
  declarations: [AppComponent, AdminLayoutComponent, ...fromShared.components],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot(),
    NxModule.forRoot(),
    ToastrModule.forRoot({
      timeOut: 2000,
      preventDuplicates: false,
      maxOpened: 5,
      closeButton: true,
      enableHtml: true,
      toastClass: 'alert alert-success alert-with-icon',
      positionClass: 'toast-top-right'
    }),
    AuthModule.forRoot(environment),
    UserModule,
    RouterModule.forRoot(AppRoutes),
    NgbModule,
    StoreModule.forRoot({}),
    EffectsModule.forRoot([]),
    environment.production ? [] : StoreDevtoolsModule.instrument()
    // StoreRouterConnectingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
