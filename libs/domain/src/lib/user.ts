export class User {
  id: number

  name?: string
  firstName: string
  middlename?: string
  lastName: string
  email: string

  role: string

  country: string
  token: string
}
