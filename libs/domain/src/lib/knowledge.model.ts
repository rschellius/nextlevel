export class KnowledgeArea {
  name: string
  description?: string
}

export class KnowledgeItem {
  name: string
  description?: string
}
