/**
 * Interface for the 'Modules' data
 */
export class Module {
  id: string
  name: string
  identity: {
    low: number
    high: number
  }
  labels: string[]
  properties: {
    studyYear: {
      low: number
      high: number
    }
    form: string
    descriptiveName: string
    name: string
    cohort: {
      low: number
      high: number
    }
    description: string
  }

  objectives?: any[]
  knowledgeItems?: any[]
  knowledgeAreas?: any[]
}
