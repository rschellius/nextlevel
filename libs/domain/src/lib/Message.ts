export class Message {
  message: string
}

export class Document {
  id: string
  doc: string
}

export const API_URL = 'api'
