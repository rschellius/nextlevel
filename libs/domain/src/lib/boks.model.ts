// import { Enti } from '@nextlevel/entity'
// import { DndNode, DndContent } from '@avans/ui'

// /**
//  * Deze class modelleert een BOKS element.
//  *
//  */
// abstract class BoksElement extends Entity {
//   name: string

//   shortDescription: string

//   longDescription: string

//   administrativeRemark: string

//   parentId: number | undefined

//   parent: BoksElement | undefined

//   year: string

//   isEditable: boolean

//   // Het 'level' (niveau) van dit BOKS-element.
//   // Toplevel elementen hebben level 0; elementen daaronder steeds 1 level hoger.
//   level: number | undefined

//   constructor(values: any = {}) {
//     super(values)
//     try {
//       this.isEditable = values.isEditable
//       this.name = values.name
//       this.year = values.year
//       this.shortDescription = values.shortDescription
//       this.longDescription = values.longDescription || undefined
//       this.administrativeRemark = values.administrativeRemark
//       this.parent = values.parent || undefined
//       this.parentId = values.parentId || undefined
//       this.level = values.level || undefined

//       // Alleen elementen op het hoogste niveau kunnen een leerlijn vormen.
//       // this.isLearningLine = !this.parent ? ( values[ 'isLearningLine' ] || false ) : false;
//     } catch (e) {
//       console.error('Error in constructor: ' + e.toString())
//     }
//   }
// }

// /**
//  *
//  */
// export class BoksNodeElement extends BoksElement implements DndNode {
//   // Een BoksNode kan subNodes bevatten.
//   // Doel is om de boomstructuur die ontstaat max 4 levels diep te houden.
//   // Hierop zal niet worden gecheckt.
//   childNodes: BoksNodeElement[] = []

//   contentNodes: BoksContentElement[] = []

//   isLearningLine = false

//   constructor(values: any = {}) {
//     super(values)
//     try {
//       this.childNodes = values.childNodes || []
//       this.contentNodes = values.contentNodes || []
//       this.isLearningLine = !this.parent ? values.isLearningLine || false : false
//     } catch (e) {
//       console.error('Error in constructor: ' + e.toString())
//     }
//   }
// }

// /**
//  *
//  */
// export class BoksContentElement extends BoksElement implements DndContent {
//   constructor(values: any = {}) {
//     super(values)
//     // try {
//     //     // Een content element moet altijd een parent hebben.
//     //     // assertNotNull( values[ 'parent' ] )
//     // } catch ( e ) {
//     //     console.error( 'Error in constructor: ' + e.toString() )
//     // }
//   }
// }
