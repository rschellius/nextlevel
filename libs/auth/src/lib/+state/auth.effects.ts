import { Injectable } from '@angular/core'
import { Router } from '@angular/router'
import { Authenticate, Error, User, UserRegistration } from '@nextlevel/domain'
import { Actions, Effect, ofType } from '@ngrx/effects'
import { Observable, of } from 'rxjs'
import { tap, map, catchError, switchMap } from 'rxjs/operators'
import { AuthService } from './../auth.service'
import * as fromAuthActions from './auth.actions'
import * as fromAlert from '@nextlevel/alert'

@Injectable()
export class AuthEffects {
  constructor(
    private readonly actions: Actions,
    private readonly authService: AuthService,
    private readonly router: Router
  ) {}

  @Effect()
  isLoggedIn$ = this.actions.pipe(
    ofType(fromAuthActions.AuthActionTypes.IS_LOGGEDIN),
    switchMap(() => {
      return this.authService.getUserFromLocalStorage().pipe(
        map((user: User) => {
          console.log('success: ', user.name)
          return new fromAuthActions.IsLoggedInSuccess(user)
        }),
        catchError((error) => {
          console.log('user not found!')
          return of(new fromAuthActions.IsLoggedInFail(error))
        })
      )
    })
  )

  @Effect({ dispatch: false })
  isLoggedInSuccess: Observable<any> = this.actions.pipe(
    ofType(fromAuthActions.AuthActionTypes.IS_LOGGEDIN_SUCCESS),
    // map((action: fromAuthActions.IsLoggedInSuccess) => action.payload),
    map(() => this.router.navigate(['/dashboard']))
  )

  @Effect({ dispatch: false })
  isLoggedInFail: Observable<any> = this.actions.pipe(
    ofType(fromAuthActions.AuthActionTypes.IS_LOGGEDIN_FAIL),
    map(() => this.router.navigate(['/login']))
  )

  @Effect()
  login: Observable<any> = this.actions.pipe(
    ofType(fromAuthActions.AuthActionTypes.LOGIN),
    map((action: fromAuthActions.Login) => action.payload),
    switchMap((payload: Authenticate) => {
      return this.authService.login(payload.email, payload.password).pipe(
        map((user: User) => {
          return new fromAuthActions.LoginSuccess(user)
        }),
        catchError((error: Error) => {
          return of(new fromAuthActions.LoginFailed(error))
        })
      )
    })
  )

  @Effect()
  loginSuccess: Observable<any> = this.actions.pipe(
    ofType(fromAuthActions.AuthActionTypes.LOGIN_SUCCESS),
    map((action: fromAuthActions.LoginSuccess) => action.payload),
    map((user: User) => this.authService.saveUserToLocalStorage(user)),
    map(
      () =>
        new fromAlert.ShowAlert({
          title: 'Logged In',
          message: 'You are logged in',
          type: fromAlert.AlertType.SUCCESS
        })
    ),
    tap(() => this.router.navigate(['/']))
  )

  @Effect()
  LoginFailed: Observable<any> = this.actions.pipe(
    ofType(fromAuthActions.AuthActionTypes.LOGIN_FAILED || fromAuthActions.AuthActionTypes.REGISTER_FAILED),
    map((action: fromAuthActions.LoginFailed) => action.payload),
    map(
      (error: Error) =>
        new fromAlert.ShowAlert({
          title: 'NOT LoggedIn',
          message: error.message,
          type: fromAlert.AlertType.ERROR
        })
    )
  )

  @Effect()
  register: Observable<any> = this.actions.pipe(
    ofType(fromAuthActions.AuthActionTypes.REGISTER),
    map((action: fromAuthActions.Register) => action.payload),
    switchMap((payload: UserRegistration) => {
      return this.authService.register(payload).pipe(
        map((user: User) => {
          return new fromAuthActions.RegisterSuccess(user)
        }),
        catchError((error: Error) => {
          return of(new fromAuthActions.RegisterFailed(error))
        })
      )
    })
  )

  @Effect()
  registerSuccess: Observable<any> = this.actions.pipe(
    ofType(fromAuthActions.AuthActionTypes.REGISTER_SUCCESS),
    map((action: fromAuthActions.RegisterSuccess) => action.payload),
    map((user: User) => this.authService.saveUserToLocalStorage(user)),
    map(
      () =>
        new fromAlert.ShowAlert({
          title: 'Registered',
          message: 'You are successfully registered',
          type: fromAlert.AlertType.SUCCESS
        })
    ),
    tap(() => this.router.navigate(['/']))
  )

  @Effect()
  LogOut: Observable<any> = this.actions.pipe(
    ofType(fromAuthActions.AuthActionTypes.LOGOUT),
    switchMap(() => this.authService.removeUserFromLocalStorage()),
    map(() => new fromAuthActions.LogOutSuccess()),
    catchError((error) => of(new fromAuthActions.LogOutFailed()))
  )

  @Effect({ dispatch: false })
  LogOutSuccess: Observable<any> = this.actions.pipe(
    ofType(fromAuthActions.AuthActionTypes.LOGOUT_SUCCESS, fromAuthActions.AuthActionTypes.LOGOUT_FAILED),
    map(() => this.router.navigate(['/login']))
  )
}
