import { Action } from '@ngrx/store'
import { Authenticate, Error, User } from '@nextlevel/domain'

export enum AuthActionTypes {
  IS_LOGGEDIN = '[Auth] IsLoggedin',
  IS_LOGGEDIN_SUCCESS = '[Auth] IsLoggedin Success',
  IS_LOGGEDIN_FAIL = '[Auth] IsLoggedin Failed',
  LOGIN = '[Auth] Login',
  LOGIN_SUCCESS = '[Auth] Login Success',
  LOGIN_FAILED = '[Auth] Login Failed',
  LOGOUT = '[Auth] Logout',
  LOGOUT_SUCCESS = '[Auth] Logout Success',
  LOGOUT_FAILED = '[Auth] Logout Failed',
  REGISTER = '[Auth] Register',
  REGISTER_SUCCESS = '[Auth] Register Success',
  REGISTER_FAILED = '[Auth] Register Failed'
}

export class IsLoggedIn implements Action {
  readonly type = AuthActionTypes.IS_LOGGEDIN
}

export class IsLoggedInSuccess implements Action {
  readonly type = AuthActionTypes.IS_LOGGEDIN_SUCCESS
  constructor(public payload: any) {}
}

export class IsLoggedInFail implements Action {
  readonly type = AuthActionTypes.IS_LOGGEDIN_FAIL
  constructor(public payload: any) {}
}

export class Login implements Action {
  readonly type = AuthActionTypes.LOGIN
  constructor(public payload: Authenticate) {}
}

export class LoginSuccess implements Action {
  readonly type = AuthActionTypes.LOGIN_SUCCESS
  constructor(public payload: User) {}
}

export class LoginFailed implements Action {
  readonly type = AuthActionTypes.LOGIN_FAILED
  constructor(public payload: Error) {}
}

export class LogOut implements Action {
  readonly type = AuthActionTypes.LOGOUT
}

export class LogOutSuccess implements Action {
  readonly type = AuthActionTypes.LOGOUT_SUCCESS
}

export class LogOutFailed implements Action {
  readonly type = AuthActionTypes.LOGOUT_FAILED
}

export class Register implements Action {
  readonly type = AuthActionTypes.REGISTER
  constructor(public payload: Authenticate) {}
}

export class RegisterSuccess implements Action {
  readonly type = AuthActionTypes.REGISTER_SUCCESS
  constructor(public payload: User) {}
}

export class RegisterFailed implements Action {
  readonly type = AuthActionTypes.REGISTER_FAILED
  constructor(public payload: Error) {}
}

export type AuthActions =
  | Login
  | LoginSuccess
  | LoginFailed
  | LogOut
  | LogOutSuccess
  | LogOutFailed
  | IsLoggedIn
  | IsLoggedInFail
  | IsLoggedInSuccess
  | Register
  | RegisterSuccess
  | RegisterFailed
