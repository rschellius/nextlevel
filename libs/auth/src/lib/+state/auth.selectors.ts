import { createFeatureSelector, createSelector } from '@ngrx/store'
import { AUTH_FEATURE_KEY, AuthenticationState, AuthState } from './auth.reducer'

export const getAuthState = createFeatureSelector<AuthenticationState>(AUTH_FEATURE_KEY)

export const getAuthInfo = createSelector(
  getAuthState,
  (state: AuthenticationState) => state[AUTH_FEATURE_KEY]
)

export const getAuthLoaded = createSelector(getAuthInfo, (state: AuthState) => state.loaded)
export const getAuthUser = createSelector(getAuthInfo, (state) => state.user)
export const getIsAuthenticated = createSelector(getAuthInfo, (state) => state.isAuthenticated)
export const getAuthToken = createSelector(getAuthInfo, (state) => state.user.token)
