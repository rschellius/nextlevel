import { User } from '@nextlevel/domain'
import { AuthActions, AuthActionTypes } from './auth.actions'

export const AUTH_FEATURE_KEY = 'auth'

/**
 * Interface for the 'Auth' data
 */
export interface AuthState {
  loading: boolean
  loaded: boolean
  user: User | null
  isAuthenticated: boolean
}

export interface AuthenticationState {
  readonly AUTH_FEATURE_KEY: AuthState
}

export const initialState: AuthState = {
  user: null,
  isAuthenticated: false,
  loaded: false,
  loading: false
}

// export const authAdapter: EntityAdapter<AuthState> = createEntityAdapter<AuthState>()

export function authReducer(state = initialState, action: AuthActions): AuthState {
  switch (action.type) {
    //
    //
    case AuthActionTypes.LOGIN:
    case AuthActionTypes.REGISTER:
      return { ...state, loading: true }
    //
    //
    case AuthActionTypes.LOGIN_SUCCESS:
    case AuthActionTypes.REGISTER_SUCCESS:
    case AuthActionTypes.IS_LOGGEDIN_SUCCESS: {
      return { ...state, user: action.payload, isAuthenticated: true, loading: false, loaded: true }
    }
    //
    //
    case AuthActionTypes.LOGIN_FAILED:
    case AuthActionTypes.REGISTER_FAILED: {
      return { ...state, user: null, isAuthenticated: false, loading: false }
    }
    //
    //
    default:
      return state
  }
}

export const reducers = {
  [AUTH_FEATURE_KEY]: authReducer
}
