import { Injectable } from '@angular/core'
import { CanActivate, ActivatedRouteSnapshot } from '@angular/router'
import { Store } from '@ngrx/store'
import { Observable } from 'rxjs'
import { tap, filter, take } from 'rxjs/operators'
import { getIsAuthenticated } from '../+state/auth.selectors'
import { AuthenticationState } from '../+state/auth.reducer'

@Injectable({ providedIn: 'root' })
export class IsLoggedInGuard implements CanActivate {
  constructor(private store: Store<AuthenticationState>) {}

  canActivate(route: ActivatedRouteSnapshot): Observable<boolean> | boolean {
    return this.store.select(getIsAuthenticated).pipe(
      tap((isAuthenticated) => {
        if (!isAuthenticated) {
          console.log('niet ingelogd')
          //
          // oplossen!
          //
          // return new fromRoot.Go({ path: ['/login'] })
        }
      }),
      // this filter construct waits for loaded to become true
      filter((isAuthenticated) => isAuthenticated),
      // this take completes the observable and unsubscribes
      take(1)
    )
  }
}
