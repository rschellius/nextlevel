// import { Injectable } from "@angular/core";
// import {
// 	Router,
// 	CanActivate,
// 	CanActivateChild,
// 	ActivatedRouteSnapshot,
// 	RouterStateSnapshot
// } from "@angular/router";
// import { Observable, of, EMPTY } from "rxjs";
// import { take, tap, mergeMap } from "rxjs/operators";
// import { AuthService } from "../services";
// import { NGXLogger } from "ngx-logger";

// const notAllowedRoute = "/not-allowed";

// // /**
// //  * Verifies that user is logged in before navigating to routes.
// //  *
// //  */
// // @Injectable()
// // export class LoggedInAuthGuard implements CanActivate, CanActivateChild {
// //   // Logging classname tag
// //   static readonly TAG = LoggedInAuthGuard.name

// //   constructor(
// //     private readonly logger: NGXLogger,
// //     private authService: AuthService,
// //     private router: Router
// //   ) {}

// //   /**
// //    *
// //    * @param route
// //    * @param state
// //    */
// //   canActivate(
// //     route: ActivatedRouteSnapshot,
// //     state: RouterStateSnapshot
// //   ): Observable<boolean> | Promise<boolean> | boolean {
// //     return this.authService. .pipe(
// //       take(1),
// //       tap(val =>
// //         this.logger.debug(
// //           LoggedInAuthGuard.TAG,
// //           `canActivate isLoggedIn = ${val}`
// //         )
// //       ),
// //       mergeMap(result => {
// //         if (result) {
// //           return of(result)
// //         } else {
// //           this.router.navigate([notAllowedRoute])
// //           return EMPTY
// //         }
// //       })
// //     )
// //   }

// //   /**
// //    *
// //    * @param route
// //    * @param state
// //    */
// //   canActivateChild(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
// //     this.logger.debug(LoggedInAuthGuard.TAG, 'canActivateChild LoggedIn')
// //     return this.canActivate(route, state)
// //   }
// // }

// /** ------------------------------------------------------------------------------
//  * Verifies that user has Admin role before navigating to routes.
//  *
//  * ------------------------------------------------------------------------------ */
// @Injectable()
// export class AdminRoleAuthGuard implements CanActivate, CanActivateChild {
// 	// Logging classname tag
// 	static readonly TAG = AdminRoleAuthGuard.name;

// 	/**
// 	 *
// 	 * @param router
// 	 * @param authService
// 	 */
// 	constructor(
// 		private readonly logger: NGXLogger,
// 		private router: Router,
// 		private authService: AuthService
// 	) {}

// 	/**
// 	 *
// 	 * @param route
// 	 * @param state
// 	 */
// 	canActivate(
// 		route: ActivatedRouteSnapshot,
// 		state: RouterStateSnapshot
// 	): Observable<boolean> | Promise<boolean> | boolean {
// 		// const url: string = state.url;
// 		return this.authService.isAdminUser.pipe(
// 			take(1),
// 			tap(val =>
// 				this.logger.debug(
// 					AdminRoleAuthGuard.TAG,
// 					`canActivate isAdmin = ${val}`
// 				)
// 			),
// 			mergeMap(result => {
// 				if (result) {
// 					return of(result);
// 				} else {
// 					this.router.navigate([notAllowedRoute]);
// 					return EMPTY;
// 				}
// 			})
// 		);
// 	}

// 	/**
// 	 *
// 	 * @param route
// 	 * @param state
// 	 */
// 	canActivateChild(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
// 		this.logger.debug(AdminRoleAuthGuard.TAG, "canActivateChild AdminRole");
// 		return this.canActivate(route, state);
// 	}
// }
