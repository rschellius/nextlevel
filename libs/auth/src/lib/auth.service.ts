import { Injectable } from '@angular/core'
import { Observable, of, EMPTY, throwError } from 'rxjs'
import { HttpClient, HttpHeaders } from '@angular/common/http'
import { tap, catchError, map } from 'rxjs/operators'
// import { NGXLogger } from 'ngx-logger'

import { EntityService } from '@nextlevel/entity'
import { CoreConfig, User, UserRegistration } from '@nextlevel/domain'

@Injectable({
  providedIn: 'root'
})
export class AuthService extends EntityService<any> {
  // Logging classname tag
  private static readonly TAG = AuthService.name

  // name of the locally stored user info in the browser db
  private static readonly LOCAL_STORAGE_TAG = 'user'

  // store the URL so we can redirect after logging in
  public readonly redirectUrl = '/dashboard'

  private readonly headers = new HttpHeaders({
    'Content-Type': 'application/json'
  })

  constructor(http: HttpClient, private config: CoreConfig) {
    super(http, config.API_URL, 'users')
  }

  login(email: string, password: string): Observable<any> {
    const API_LOGIN = this.config.API_URL + 'auth/login'
    console.log(AuthService.TAG, 'login at ', API_LOGIN)
    // return this.http.post(API_LOGIN, { email, password }, { headers: this.headers }).pipe(
    //   map((result) => result as User),
    //   catchError(this.handleError)
    // )
    return of({
      firstName: 'Robin',
      lastName: 'Schellius',
      name: 'Robin Schellius',
      id: '735b3c2d-c14b-4899-9a6f-a66529b23974',
      email: 'r.schellius@avans.nl',
      token:
        'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJmaXJzdE5hbWUiOiJSb2JpbiIsImxhc3ROYW1lIjoiU2NoZWxsaXVzIiwibmFtZSI6IlJvYmluIFNjaGVsbGl1cyIsImlkIjoiNzM1YjNjMmQtYzE0Yi00ODk5LTlhNmYtYTY2NTI5YjIzOTc0IiwiZW1haWwiOiJyLnNjaGVsbGl1c0BhdmFucy5ubCIsImlhdCI6MTYwNTgxNDMwNiwiZXhwIjoxNjA4NDA2MzA2fQ.2iI8R68yY-cNPeXSk_GAHS1BmQqYg70SejoLmgnW7qk'
    })
  }

  register(user: UserRegistration): Observable<User> {
    console.log(AuthService.TAG, 'login')
    const API_REGISTER = this.config.API_URL + 'auth/register'
    console.log(AuthService.TAG, 'login at ', API_REGISTER)
    return this.http.post(API_REGISTER, user, { headers: this.headers }).pipe(
      map((result) => result as User),
      catchError(this.handleError)
    )
  }

  public removeUserFromLocalStorage(): Observable<boolean> {
    console.log(AuthService.TAG, 'removing local user')
    localStorage.removeItem(AuthService.LOCAL_STORAGE_TAG)
    return of(true)
  }

  public getUserFromLocalStorage(): Observable<User> {
    console.log('getuser from local storage')
    const user = localStorage.getItem(AuthService.LOCAL_STORAGE_TAG)
    if (!user || user === null) {
      console.log('No user in local storage')
      return throwError({ message: 'No user in local storage' })
    } else {
      console.log('Found', (JSON.parse(user) as User).name)
      return of(JSON.parse(user) as User)
    }
  }

  public saveUserToLocalStorage(user: User): Observable<boolean> {
    localStorage.setItem(AuthService.LOCAL_STORAGE_TAG, JSON.stringify(user))
    return of(true)
  }
}
