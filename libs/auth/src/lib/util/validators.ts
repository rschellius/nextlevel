import { FormControl } from '@angular/forms'

export function validEmail(control: FormControl): { [s: string]: boolean } {
  const email = control.value
  const regexp = new RegExp('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')
  if (regexp.test(email) !== true) {
    return { email: false }
  } else {
    return null
  }
}

export function validPassword(control: FormControl): { [s: string]: boolean } {
  const password = control.value
  const regexp = new RegExp('^[a-zA-Z]([a-zA-Z0-9]){2,14}')
  const test = regexp.test(password)
  if (regexp.test(password) !== true) {
    return { password: false }
  } else {
    return null
  }
}
