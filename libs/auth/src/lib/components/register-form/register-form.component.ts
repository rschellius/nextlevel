import { Component, OnInit, ChangeDetectionStrategy, Output, EventEmitter } from '@angular/core'
import { FormControl, FormGroup, Validators } from '@angular/forms'
import { Authenticate, User, UserRegistration } from '@nextlevel/domain'
import { validEmail, validPassword } from '../../util/validators'

@Component({
  selector: 'nextlevel-register-form',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './register-form.component.html',
  styles: ['../login/login-form.component.css']
})
export class RegisterFormComponent implements OnInit {
  registerForm: FormGroup | undefined

  @Output()
  authentication = new EventEmitter<Authenticate>()

  constructor() {}

  ngOnInit() {
    this.registerForm = new FormGroup({
      email: new FormControl('', [Validators.required, validEmail.bind(this)]),
      password: new FormControl('', [Validators.required, validPassword.bind(this)]),
      firstName: new FormControl('', [Validators.required]),
      lastName: new FormControl('', [Validators.required])
    })
  }

  onSubmit() {
    if (this.registerForm.valid) {
      const firstName = this.registerForm.value['firstName']
      const lastName = this.registerForm.value['lastName']
      const email = this.registerForm.value['email']
      const password = this.registerForm.value['password']
      const inputFields: UserRegistration = { firstName, lastName, email, password }
      this.authentication.emit(inputFields)
    }
  }
}
