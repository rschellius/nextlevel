import { Component, OnInit, ChangeDetectionStrategy, Input, Output, EventEmitter } from '@angular/core'
import { FormControl, FormGroup, Validators } from '@angular/forms'
import { Authenticate } from '@nextlevel/domain'
import { validEmail, validPassword } from '../../util/validators'

@Component({
  selector: 'nextlevel-login-form',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './login-form.component.html',
  styles: ['./login-form.component.css']
})
export class LoginFormComponent implements OnInit {
  loginForm: FormGroup | undefined

  @Output()
  authentication = new EventEmitter<Authenticate>()

  constructor() {}

  ngOnInit() {
    this.loginForm = new FormGroup({
      email: new FormControl(null, [Validators.required, validEmail.bind(this)]),
      password: new FormControl(null, [Validators.required, validPassword.bind(this)])
    })
  }

  onSubmit() {
    if (this.loginForm.valid) {
      const email = this.loginForm.value['email']
      const password = this.loginForm.value['password']
      const inputFields: Authenticate = { email, password }
      this.authentication.emit(inputFields)
    }
  }
}
