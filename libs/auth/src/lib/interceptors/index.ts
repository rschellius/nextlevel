/*
"Barrel" of Http Interceptors

https://angular.io/guide/http#provide-the-interceptor

*/
import { HTTP_INTERCEPTORS } from '@angular/common/http'

import { AuthInterceptor } from './auth.interceptor'

/** Http interceptor providers in outside-in order */
export const httpInterceptorProviders = [
  { provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true }
]
