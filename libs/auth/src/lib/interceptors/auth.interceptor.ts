import { HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http'
import { Injectable } from '@angular/core'
import { Store } from '@ngrx/store'
import { Subscription } from 'rxjs'
import { first, flatMap, mergeMap } from 'rxjs/operators'
import { AuthenticationState } from '../+state/auth.reducer'
import { getAuthToken } from '../+state/auth.selectors'

/**
 * With interception, you declare interceptors that inspect and transform HTTP
 * requests from your application to a server. The same interceptors can also
 * inspect and transform a server's responses on their way back to the
 * application. Multiple interceptors form a forward-and-backward chain
 * of request/response handlers.
 *
 * https://angular.io/guide/http#intercepting-requests-and-responses
 */
@Injectable()
export class AuthInterceptor implements HttpInterceptor {
  //
  //
  constructor(private readonly store: Store<AuthenticationState>) {}

  //
  // https://antonyderham.me/post/angular-ngrx-auth-interceptor/
  //
  intercept(req: HttpRequest<any>, next: HttpHandler) {
    // Get the auth token from the ngrx store.
    return this.store.select(getAuthToken).pipe(
      first(),
      mergeMap((token) => {
        const authReq = !!token
          ? req.clone({
              setHeaders: { Authorization: 'Bearer ' + token }
            })
          : req

        return next.handle(authReq)
      })
    )
  }
}
