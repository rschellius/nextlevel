import { ModuleWithProviders, NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { RouterModule, Route } from '@angular/router'

import * as fromComponents from './components'
import * as fromContainers from './containers'
import { StoreModule } from '@ngrx/store'
import { EffectsModule } from '@ngrx/effects'
import { reducers, AUTH_FEATURE_KEY } from './+state/auth.reducer'
import { AuthEffects } from './+state/auth.effects'
import { HttpClientModule } from '@angular/common/http'
import { CoreConfig } from '@nextlevel/domain'
import { AlertModule } from '@nextlevel/alert'
import { ReactiveFormsModule } from '@angular/forms'
import { httpInterceptorProviders } from './interceptors'

export const authRoutes: Route[] = [
  // { path: 'login', component: fromContainers.LoginComponent },
  // { path: 'register', component: fromContainers.RegisterComponent }
]

@NgModule({
  declarations: [...fromComponents.components, ...fromContainers.containers],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    RouterModule.forChild(authRoutes),
    HttpClientModule,
    AlertModule,
    StoreModule.forFeature(AUTH_FEATURE_KEY, reducers),
    EffectsModule.forFeature([AuthEffects])
  ],
  exports: [...fromComponents.components],
  providers: [AuthEffects, httpInterceptorProviders]
})
export class AuthModule {
  static forRoot(config: CoreConfig | null | undefined): ModuleWithProviders<AuthModule> {
    return {
      ngModule: AuthModule,
      providers: [
        { provide: CoreConfig, useValue: config || {} }
        // { provide: RouterStateSerializer, useClass: CustomSerializer }
      ]
    }
  }
}
