import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core'
import { FormControl, FormGroup, Validators } from '@angular/forms'
import { Store } from '@ngrx/store'
import { Authenticate } from '@nextlevel/domain'
import { AuthenticationState } from '../../+state/auth.reducer'
import { Login } from '../../+state/auth.actions'

@Component({
  selector: 'nextlevel-login-container',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup | undefined

  constructor(private store: Store<AuthenticationState>) {}

  ngOnInit() {}

  onSubmit(event: any) {
    this.store.dispatch(new Login(event))
  }
}
