import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { StoreModule } from '@ngrx/store'
import { EffectsModule } from '@ngrx/effects'
import * as fromAlert from './+state/alert.reducer'
import { AlertEffects } from './+state/alert.effects'
import { ToastrModule } from 'ngx-toastr'

@NgModule({
  imports: [
    CommonModule,
    ToastrModule.forRoot({
      timeOut: 3000,
      closeButton: true,
      enableHtml: true,
      toastClass: 'alert alert-success alert-with-icon',
      positionClass: 'toast-bottom-left'
    }),
    StoreModule.forFeature(fromAlert.ALERT_FEATURE_KEY, fromAlert.alertReducer, {
      initialState: fromAlert.initialState
    }),
    EffectsModule.forFeature([AlertEffects])
  ]
})
export class AlertModule {}
