import { EntityAdapter, createEntityAdapter } from '@ngrx/entity'
import { AlertActionTypes, AlertActions } from './alert.actions'
import { AlertEntity } from './alert.models'

export const ALERT_FEATURE_KEY = 'alert'

export interface AlertState {
  readonly [ALERT_FEATURE_KEY]: AlertEntity
}

export const initialState: AlertEntity = {
  type: null,
  title: undefined,
  message: undefined
}

export const alertAdapter: EntityAdapter<AlertEntity> = createEntityAdapter<AlertEntity>()

export function alertReducer(state = initialState, action: AlertActions): AlertEntity {
  switch (action.type) {
    case AlertActionTypes.SHOW_ALERT:
      return {
        ...state,
        title: action.payload.title,
        message: action.payload.message,
        type: action.payload.type
      }

    case AlertActionTypes.SHOW_ALERT_DONE: {
      return { ...state }
    }

    case AlertActionTypes.SHOW_ALERT_FAILED: {
      return { ...state, message: undefined, type: undefined }
    }

    default:
      return state
  }
}
