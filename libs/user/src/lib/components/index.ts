import { UserFormComponent } from './user-form/user-form.component'
import { UserNavDropdownComponent } from './user-nav-dropdown/user-nav-dropdown.component'

export const components: any[] = [UserFormComponent, UserNavDropdownComponent]

export * from './user-form/user-form.component'
export * from './user-nav-dropdown/user-nav-dropdown.component'
