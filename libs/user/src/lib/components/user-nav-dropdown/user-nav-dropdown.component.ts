import { Component, OnInit } from '@angular/core'
import { Store } from '@ngrx/store'
import { AuthenticationState, LogOut } from '@nextlevel/auth'

@Component({
  selector: 'nextlevel-user-nav-dropdown',
  templateUrl: './user-nav-dropdown.component.html'
})
export class UserNavDropdownComponent implements OnInit {
  constructor(private readonly store: Store<AuthenticationState>) {}

  ngOnInit(): void {}

  logout() {
    this.store.dispatch(new LogOut())
  }
}
