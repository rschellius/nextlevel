import { Action } from '@ngrx/store'
import { User } from '@nextlevel/domain'

export enum UserActionTypes {
  LOAD_USERS = '[User] Load Users',
  LOAD_USERS_SUCCESS = '[User] Load Users Success',
  LOAD_USERS_FAILED = '[User] Load Users Failed'
}

export class LoadUsers implements Action {
  readonly type = UserActionTypes.LOAD_USERS
}

export class LoadUsersSuccess implements Action {
  readonly type = UserActionTypes.LOAD_USERS_SUCCESS
  constructor(public payload: User) {}
}

export class LoadUsersFailed implements Action {
  readonly type = UserActionTypes.LOAD_USERS
  constructor(public payload: any) {}
}

export type UserActions = LoadUsers | LoadUsersFailed | LoadUsersSuccess
