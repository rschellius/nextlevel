// import { EntityState, EntityAdapter, createEntityAdapter } from '@ngrx/entity'
// import { AuthActions, AuthActionTypes } from './user.actions'
// import { AuthState } from './user.models'
// import { User } from '@nextlevel/domain'

// export const USERS_FEATURE_KEY = 'users'

// export interface AuthenticationState {
//   readonly [USERS_FEATURE_KEY]: AuthState
// }

// export const initialState: AuthState = {
//   id: 0,
//   error: '',
//   user: null,
//   isAuthenticated: false,
//   loaded: false,
//   loading: false
// }

// export const authAdapter: EntityAdapter<AuthState> = createEntityAdapter<AuthState>()

// export function authReducer(state = initialState, action: AuthActions): AuthState {
//   switch (action.type) {
//     //
//     //
//     case AuthActionTypes.IS_LOGGEDIN:
//       return { ...state, loading: true }
//     //
//     //
//     case AuthActionTypes.IS_LOGGEDIN_SUCCESS: {
//       return { ...state, user: action.payload, isAuthenticated: true, loading: false, loaded: true }
//     }
//     //
//     //
//     case AuthActionTypes.IS_LOGGEDIN_FAILED: {
//       return { ...state, user: null, isAuthenticated: false, loading: false }
//     }
//     //
//     //
//     default:
//       return state
//   }
// }
