import { Injectable } from '@angular/core'
import { Router } from '@angular/router'
import { Authenticate, Error, User } from '@nextlevel/domain'
import { Actions, Effect, ofType } from '@ngrx/effects'
import { Observable, of } from 'rxjs'
import { tap, map, catchError, switchMap } from 'rxjs/operators'
import { UserService } from './../user.service'
import * as fromAuthActions from './user.actions'
import * as fromAlert from '@nextlevel/alert'

@Injectable()
export class UserEffects {
  constructor(private readonly actions: Actions, private readonly router: Router) {}

  // @Effect()
  // isLoggedIn$ = this.actions.pipe(
  //   ofType(fromAuthActions.AuthActionTypes.IS_LOGGEDIN),
  //   // switchMap(() => this.authService.getUserFromLocalStorage()),
  //   map((user: User) => new fromAuthActions.IsLoggedInSuccess(user)),
  //   catchError((error) => of(new fromAuthActions.IsLoggedInFailed(error)))
  // )

  // @Effect({ dispatch: false })
  // isLoggedInSuccess: Observable<any> = this.actions.pipe(
  //   ofType(fromAuthActions.AuthActionTypes.IS_LOGGEDIN_SUCCESS),
  //   map((action: fromAuthActions.IsLoggedInSuccess) => action.payload)
  // )

  // @Effect({ dispatch: false })
  // isLoggedInFail: Observable<any> = this.actions.pipe(
  //   ofType(fromAuthActions.AuthActionTypes.IS_LOGGEDIN_FAILED),
  //   tap(console.log),
  //   map(() => this.router.navigate(['/login']))
  // )
}
