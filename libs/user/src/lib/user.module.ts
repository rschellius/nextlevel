import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { RouterModule, Route } from '@angular/router'
import { StoreModule } from '@ngrx/store'
import { NgbModule } from '@ng-bootstrap/ng-bootstrap'
import { EffectsModule } from '@ngrx/effects'
// import * as fromReducer from './+state/user.reducer'
import { UserEffects } from './+state/user.effects'
import * as fromComponents from './components'
import * as fromContainers from './containers'

export const userRoutes: Route[] = []

@NgModule({
  declarations: [...fromComponents.components, ...fromContainers.containers],
  imports: [
    CommonModule,
    RouterModule,
    NgbModule
    // StoreModule.forFeature(fromReducer.USERS_FEATURE_KEY, fromReducer.authReducer),
    // EffectsModule.forFeature([UserEffects])
  ],
  exports: [...fromComponents.components, ...fromContainers.containers]
})
export class UserModule {}
