import { Injectable } from '@angular/core'
import { Observable, of, EMPTY, throwError } from 'rxjs'
import { HttpClient, HttpHeaders } from '@angular/common/http'
import { tap, catchError, map } from 'rxjs/operators'
import { EntityService } from '@nextlevel/entity'
import { CoreConfig } from '@nextlevel/domain'

@Injectable({
  providedIn: 'root'
})
export class UserService extends EntityService<any> {
  // Logging classname tag
  static readonly TAG = UserService.name

  private readonly headers = new HttpHeaders({
    'Content-Type': 'application/json'
  })

  constructor(http: HttpClient, private config: CoreConfig) {
    super(http, config.API_URL, 'users')
  }
}
